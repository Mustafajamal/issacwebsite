<?php
add_filter( 'woocommerce_product_tabs', 'm_remove_additional_information' );
 
function m_remove_additional_information( $tabs ) {
	unset( $tabs['additional_information'] );
    unset( $tabs['description'] );
	return $tabs;
}
// define the woocommerce_after_single_product_summary callback 
//add_shortcode( 'choptoolold', 'wpdocs_chop_tool' );
function wpdocs_chop_tool( ) { 
    global $product;
    $product_id = $product->get_id();
    ob_start();
    ?>
    <div class='row single_product_tool'>
        <div class="design-area size">
            <a class="button primary lowercase designarea-size-inner" style="padding:0p 0px 0px 0px;">
                <span class="left-info-icon"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="15" viewBox="0 0 12.003 24.999"><path id="info" d="M444.642,69.3l-.38,1.428q-1.708.62-2.725.945a7.774,7.774,0,0,1-2.364.325,5,5,0,0,1-3.215-.928,2.907,2.907,0,0,1-1.147-2.358,7.679,7.679,0,0,1,.086-1.135,12.873,12.873,0,0,1,.277-1.309l1.423-4.636q.19-.666.32-1.262a5.06,5.06,0,0,0,.13-1.086,1.594,1.594,0,0,0-.4-1.239,2.394,2.394,0,0,0-1.522-.35,4.292,4.292,0,0,0-1.131.159c-.387.105-.718.207-1,.3l.381-1.43q1.4-.523,2.676-.9a8.646,8.646,0,0,1,2.42-.374,4.869,4.869,0,0,1,3.167.914,2.925,2.925,0,0,1,1.113,2.372c0,.2-.024.556-.077,1.064a6.612,6.612,0,0,1-.285,1.4l-1.417,4.617a11.432,11.432,0,0,0-.311,1.272,6.044,6.044,0,0,0-.139,1.079,1.484,1.484,0,0,0,.446,1.254,2.691,2.691,0,0,0,1.55.333,4.838,4.838,0,0,0,1.172-.167A7.021,7.021,0,0,0,444.642,69.3ZM445,49.919a2.611,2.611,0,0,1-.991,2.056,3.542,3.542,0,0,1-2.387.851,3.583,3.583,0,0,1-2.4-.851,2.619,2.619,0,0,1,0-4.119,3.77,3.77,0,0,1,4.786,0A2.635,2.635,0,0,1,445,49.919Z" transform="translate(-432.998 -47)" fill="#181818" style="fill: white;"></path></svg></span>  <span class="right-info-text">The size of the design area is <span class="size-sel-value">30 x 40</span></span>
            </a>
        </div>
        <div class="inside-container-create-d">
            <a class="button primary lowercase create-personal-design" style="">
                <img id="close-design-tool" src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/Cross.png">  <span>Create Personal Design</span>
            </a>
        </div>
        <div class='col small-12 large-9'>
            <div class="engraving_filters_parent" style="display: block;"><div class="engraving_filters"></div></div>
    		<?php //echo do_shortcode('[fpd]'); ?>          
    	</div>  	
        <div class='tool-right col small-12 large-3'>  		
            
            <div class="select-side-fancy" style="display: none;">
                <div class='label'>Select Side</div>
                <div class='tool-right-views'></div>            
            </div>

                <div class='select-side-wrap'> 
                    <div class='select-side-inner'> 
                        <div class='label select-side'>Select Side</div>
                        <label for="sideb"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/cooking-board.png"> Side B <input type="radio" id="sideb" name="select_side" value="sideb" checked></label>
                        <label for="sidea"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/cooking-board.png"> Side A <input type="radio" id="sidea" name="select_side" value="sidea"></label>                        
                    </div>                    
                </div>
              
                <div class='select-size-wrap'> 
                    <div class='label'>Select Size of the product(cm)</div>
                    <div class='select-size-varaitons'> 
                        <?php echo do_shortcode('[add_to_cart_form id="'.$product_id.'"] '); ?>
                    </div>                    
                </div>                    

            <!-- //Upload Image Button -->
            <a class='upload-image-button tool-buttons main-actions'><span class='tool-buttons-icon'><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='23' height='21' viewBox='0 0 23 21'><defs><filter id='filter' x='1465' y='1333' width='23' height='21' filterUnits='userSpaceOnUse'><feFlood result='flood' flood-color='#fff'/><feComposite result='composite' operator='in' in2='SourceGraphic'/><feBlend result='blend' in2='SourceGraphic'/></filter></defs><path id='Forma_1' data-name='Forma 1' class='cls-1' d='M1476.5,1339a5.68,5.68,0,1,0,5.62,5.68A5.657,5.657,0,0,0,1476.5,1339Zm0,9.54a3.86,3.86,0,1,1,3.82-3.86A3.841,3.841,0,0,1,1476.5,1348.54Zm8.8-12.27H1482a0.282,0.282,0,0,1-.25-0.15l-0.91-1.94c0-.01-0.01-0.01-0.01-0.02a2.057,2.057,0,0,0-1.86-1.16h-4.88a2.067,2.067,0,0,0-1.86,1.16v0.02l-0.92,1.94a0.282,0.282,0,0,1-.25.15h-3.36a2.721,2.721,0,0,0-2.7,2.73v12.27a2.714,2.714,0,0,0,2.7,2.73h17.6a2.714,2.714,0,0,0,2.7-2.73V1339A2.721,2.721,0,0,0,1485.3,1336.27Zm0.9,15a0.9,0.9,0,0,1-.9.91h-17.6a0.9,0.9,0,0,1-.9-0.91V1339a0.9,0.9,0,0,1,.9-0.91h3.36a2.063,2.063,0,0,0,1.86-1.16c0-.01.01-0.01,0.01-0.02l0.91-1.94a0.275,0.275,0,0,1,.25-0.15h4.88a0.265,0.265,0,0,1,.25.15l0.91,1.94a0.02,0.02,0,0,1,.01.02,2.085,2.085,0,0,0,1.86,1.16h3.3a0.9,0.9,0,0,1,.9.91v12.27Zm-3.59-11.36h1.8v1.82h-1.8v-1.82Z' transform='translate(-1465 -1333)' style="fill: white;" /></svg></span><span class='tool-buttons-text'>Upload a Photo</span></a>

            <!--//Add Text -->
            <a class='add-text-button tool-buttons main-actions'><span class='tool-buttons-icon'><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='31' height='18' viewBox='0 0 31 18'><image id='L1_copy' data-name='L1 copy' width='31' height='18' xlink:href='data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAASCAQAAAAJOc1sAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflAhcLKSfQStm2AAACWUlEQVQ4y5WSa2gNYBzGfzsdOUlLIyuXtJgkVku0tJSSXJpbpORSiokoWZSiNMklKYRlND7gi/mAEvKBFKJt1iY5jm2nXWzOOWu26czm58POdCaKni//93361fO8/xdJ03q77LbLpBXD7v+q9EPIt35wo6u8bZsz/hdf7le3iDjKRk/+L37NiKNTc6UNv+Z/wscZ9px43udioT2pJEMa43nf+dGPhv3gQ+cPx7eaMMeQnX53sljjqzR4pmFjPvKgW9zjLZvtdGM6Xm21WGLCiNfFDXaYnfIyrbbebLHEMnem6rWZOwTn2+MG8YUPPGS7WeIny1NusUkPONVqW3xps08NON24u4bwi0bFXFstMs+E+8QKO1LuSqO2GbPZPLMssd8lBmzx1KAdTC2qzEanGPKVVQbNt8uDv8ple9RyH9hku/VOFVs9GwRgO5lcBQqYwF36mEYf2VRRy2qOAbCQM4wlRgNPCPOEKCFgIADADhoIA6VcoYZ33OYwCaCciRQAs7jBaHZTRJRJbOMxBfQDA4iFdrrrj98i5CevipVGDDjXmHVecJtJ1xm01ZNBYD1xbgL3mUcvGQAEGGANb3jOMnKZQRU/2E+Q2fxgE0nqBsMHyWQxT4lTxALOEWEkIH0c5wSLOM0KltLLCOAhSyijm83coZ4QGYRwkzHniM98Pyz4Pj+L+MjHHrfNcWKxtb7xiJfMF+usxNc2meNae9KWNNi7xTLz3GvcQmv9Yqm55nnEWr8bttRG4xi113a7bTL428NdNmmHX/xmseO9Z6cJ47Zb4VojRo1a8xNPdndRQLs9TQAAAABJRU5ErkJggg=='/></svg></span><span class='tool-buttons-text'>Add a Text</span></a>
    		
            <!--//Select Icon -->
            <a class='select-icon-button tool-buttons main-actions'><span class='tool-buttons-icon'><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/select_icon.png"></span><span class='tool-buttons-text'>Select an Icon</span></a>

            <!--//Upload Image Content --> 
            <div class='upload-image-content'>
			      <span class="container-popup-close">X</span>
                  <h3>Select a Picture</h3>
                  <p>By uploading the image i confirm that the image* rights belongs to me.</p>
            <?php echo do_shortcode('[fpd_module type="images"]'); ?>
		    </div>


            
            <!--// Add text content -->
            <div class='add-text-content'>  
                    <span class="container-popup-close">X</span>
                    <h4>Enter Text</h4> 
                <?php echo do_shortcode('[fpd_module type="text-layers"][fpd_module type="text"]'); ?>
            </div>

        
            <!-- // Select Icon content -->
            <div class='select-icon-content'>
                <span class="container-popup-close">X</span>
                <h3>Select a Category</h3>
                <?php echo do_shortcode('[fpd_module type="designs"]'); ?>
            </div>   

        </div>
        <div class="inside-container-bottom-close">
            <a class="close-icon-bottom" style="padding:0p 0px 0px 0px;"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/Cross.png"></a>
        </div>  	
    </div>
    <?php
    $content = ob_get_contents();
    ob_end_clean();
   return $content;
} 



function add_to_cart_form_shortcode( $atts ) {
        if ( empty( $atts ) ) {
            return '';
        }

        if ( ! isset( $atts['id'] ) && ! isset( $atts['sku'] ) ) {
            return '';
        }

        $args = array(
            'posts_per_page'      => 1,
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
            'no_found_rows'       => 1,
        );

        if ( isset( $atts['sku'] ) ) {
            $args['meta_query'][] = array(
                'key'     => '_sku',
                'value'   => sanitize_text_field( $atts['sku'] ),
                'compare' => '=',
            );

            $args['post_type'] = array( 'product', 'product_variation' );
        }

        if ( isset( $atts['id'] ) ) {
            $args['p'] = absint( $atts['id'] );
        }

        $single_product = new WP_Query( $args );

        $preselected_id = '0';


        if ( isset( $atts['sku'] ) && $single_product->have_posts() && 'product_variation' === $single_product->post->post_type ) {

            $variation = new WC_Product_Variation( $single_product->post->ID );
            $attributes = $variation->get_attributes();


            $preselected_id = $single_product->post->ID;


            $args = array(
                'posts_per_page'      => 1,
                'post_type'           => 'product',
                'post_status'         => 'publish',
                'ignore_sticky_posts' => 1,
                'no_found_rows'       => 1,
                'p'                   => $single_product->post->post_parent,
            );

            $single_product = new WP_Query( $args );
        ?>
            <script type="text/javascript">
                jQuery( document ).ready( function( $ ) {
                    var $variations_form = $( '[data-product-page-preselected-id="<?php echo esc_attr( $preselected_id ); ?>"]' ).find( 'form.variations_form' );
                    <?php foreach ( $attributes as $attr => $value ) { ?>
                        $variations_form.find( 'select[name="<?php echo esc_attr( $attr ); ?>"]' ).val( '<?php echo esc_js( $value ); ?>' );
                    <?php } ?>
                });
            </script>
        <?php
        }

        $single_product->is_single = true;
        ob_start();
        global $wp_query;

        $previous_wp_query = $wp_query;

        $wp_query          = $single_product;

        wp_enqueue_script( 'wc-single-product' );
        while ( $single_product->have_posts() ) {
            $single_product->the_post()
            ?>
            <div class="single-product" data-product-page-preselected-id="<?php echo esc_attr( $preselected_id ); ?>">
                <?php woocommerce_template_single_add_to_cart(); ?>
            </div>
            <?php
        }

        $wp_query = $previous_wp_query;

        wp_reset_postdata();
        return '<div class="woocommerce">' . ob_get_clean() . '</div>';
}
add_shortcode( 'add_to_cart_form', 'add_to_cart_form_shortcode' );

/*Example Usage [add_to_cart_form id=147]*/


function variation_radio_buttons($html, $args) {
  $args = wp_parse_args(apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args), array(
    'options'          => false,
    'attribute'        => false,
    'product'          => false,
    'selected'         => false,
    'name'             => '',
    'id'               => '',
    'class'            => '',
    'show_option_none' => __('Choose an option', 'woocommerce'),
  ));

  if(false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product) {
    $selected_key     = 'attribute_'.sanitize_title($args['attribute']);
    $args['selected'] = isset($_REQUEST[$selected_key]) ? wc_clean(wp_unslash($_REQUEST[$selected_key])) : $args['product']->get_variation_default_attribute($args['attribute']);
  }

  $options               = $args['options'];
  $product               = $args['product'];
  $attribute             = $args['attribute'];
  $name                  = $args['name'] ? $args['name'] : 'attribute_'.sanitize_title($attribute);
  $id                    = $args['id'] ? $args['id'] : sanitize_title($attribute);
  $class                 = $args['class'];
  $show_option_none      = (bool)$args['show_option_none'];
  $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce');

  if(empty($options) && !empty($product) && !empty($attribute)) {
    $attributes = $product->get_variation_attributes();
    $options    = $attributes[$attribute];
  }

  $radios = '<div class="variation-radios">';

  if(!empty($options)) {
    if($product && taxonomy_exists($attribute)) {
      $terms = wc_get_product_terms($product->get_id(), $attribute, array(
        'fields' => 'all',
      ));

      foreach($terms as $term) {
        if(in_array($term->slug, $options, true)) {
          $id = $name.'-'.$term->slug;
          $radios .= '<div class="select-size-main"><label for="'.esc_attr($id).'">'.esc_html(apply_filters('woocommerce_variation_option_name', $term->name)).'</label><input type="radio" id="'.esc_attr($id).'" name="'.esc_attr($name).'" value="'.esc_attr($term->slug).'" '.checked(sanitize_title($args['selected']), $term->slug, false).'></div>';
        }
      }
    } else {
      foreach($options as $option) {
        $id = $name.'-'.$option;
        $checked    = sanitize_title($args['selected']) === $args['selected'] ? checked($args['selected'], sanitize_title($option), false) : checked($args['selected'], $option, false);
        $radios    .= '<div class="select-size-main"><label for="'.esc_attr($id).'">'.esc_html(apply_filters('woocommerce_variation_option_name', $option)).'</label><input type="radio" id="'.esc_attr($id).'" name="'.esc_attr($name).'" value="'.esc_attr($option).'" id="'.sanitize_title($option).'" '.$checked.'></div>';
      }
    }
  }

  $radios .= '</div>';
    
  return $html.$radios;
}
add_filter('woocommerce_dropdown_variation_attribute_options_html', 'variation_radio_buttons', 20, 2);

function variation_check($active, $variation) {
  if(!$variation->is_in_stock() && !$variation->backorders_allowed()) {
    return false;
  }
  return $active;
}
add_filter('woocommerce_variation_is_active', 'variation_check', 10, 2);

/** excerpt position change **/

//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 50 );

// function product_category_filter_changes()
// {
//     remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 20 );
//     add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5 );
// }
// add_action('template_redirect','product_category_filter_changes');

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

add_shortcode( 'selsize', 'wpdocs_select_size' );
function wpdocs_select_size() {
    echo "sdfsdf";
    do_action("woocommerce_tm_epo");
}
//Add Custom price to Cart
function woocommerce_custom_price_to_cart_item( $cart_object ) {  
    if( !WC()->session->__isset( "reload_checkout" )) {
        foreach ( $cart_object->cart_contents as $key => $value ) {
            if( isset( $value["custom_price"] ) ) {
                //for woocommerce version lower than 3
                //$value['data']->price = $value["custom_price"];
                //for woocommerce version +3
                $value['data']->set_price($value["custom_price"]);
            }
        }  
    }  
}
add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );
