<?php
/**
 * @package Insurance_Form
 * @version 1.0.0
 */
/*
Plugin Name: Insurance Form
Plugin URI: http://logicsbuffer.com/
Description: Insurance form Plugin shortcode [insform]
Author: Logics Buffer Team
Version: 1.0.0
Author URI: http://logicsbuffer.com/
*/

add_action( 'init', 'insurance_form' );

function insurance_form() {
	add_action( 'wp_enqueue_scripts', 'insurance_form_script' );
}
function insurance_form_script() {		        
	wp_enqueue_style( 'insurance_form_maincss', plugins_url().'/step-form/style.css',array(),time());
}

// This just echoes the chosen line, we'll position it later.
function insuranceForm() {

ob_start();
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<div class="form-main">
		<div class="step1 stepmain" step-val="1">
			<div class="container">
				<div class="row">
					<div class="col col-sm-3 ">
				  	</div>
					<div class="col col-sm-6">
						<div class="form-group">
					  	<label for="usr">What is your ZIP Code?</label>
					  	<input type="text" class="form-control" id="zipcode">
						</div>
						<button type="button" id="step1_submit" data-val="1" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-3 ">
						</div>
				</div>
			</div>
		</div>
		<div class="step2 stepmain" step-val="2">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="form-group">
							<div class="row">
								<h1 style="text-align:center;">What is your vehicle year?</h1>
								<div class="col col-sm-2">
									<input type="radio" id="year-2022" name="year" class="pbtpt4-1 kBNAEq" value="2022">
									<label for="year-2022" class="pbtpt4-0 fcerNr">2022</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2021" name="year" class="pbtpt4-1 kBNAEq" value="2021">
									<label for="year-2021" class="pbtpt4-0 fcerNr">2021</label>
							  </div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2020" name="year" class="pbtpt4-1 kBNAEq" value="2020">
									<label for="year-2020" class="pbtpt4-0 fcerNr">2020</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2019" name="year" class="pbtpt4-1 kBNAEq" value="2019">
									<label for="year-2019" class="pbtpt4-0 fcerNr">2019</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2018" name="year" class="pbtpt4-1 kBNAEq" value="2018">
									<label for="year-2018" class="pbtpt4-0 fcerNr">2018</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2017" name="year" class="pbtpt4-1 kBNAEq" value="2017">
									<label for="year-2017" class="pbtpt4-0 fcerNr">2017</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2016" name="year" class="pbtpt4-1 kBNAEq" value="2016">
									<label for="year-2016" class="pbtpt4-0 fcerNr">2016</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2015" name="year" class="pbtpt4-1 kBNAEq" value="2015">
									<label for="year-2015" class="pbtpt4-0 fcerNr">2015</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2014" name="year" class="pbtpt4-1 kBNAEq" value="2014">
									<label for="year-2014" class="pbtpt4-0 fcerNr">2014</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2013" name="year" class="pbtpt4-1 kBNAEq" value="2013">
									<label for="year-2013" class="pbtpt4-0 fcerNr">2013</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2012" name="year" class="pbtpt4-1 kBNAEq" value="2012">
									<label for="year-2012" class="pbtpt4-0 fcerNr">2012</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2011" name="year" class="pbtpt4-1 kBNAEq" value="2011">
									<label for="year-2011" class="pbtpt4-0 fcerNr">2011</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2010" name="year" class="pbtpt4-1 kBNAEq" value="2010">
									<label for="year-2010" class="pbtpt4-0 fcerNr">2010</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2009" name="year" class="pbtpt4-1 kBNAEq" value="2009">
									<label for="year-2009" class="pbtpt4-0 fcerNr">2009</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2008" name="year" class="pbtpt4-1 kBNAEq" value="2008">
									<label for="year-2008" class="pbtpt4-0 fcerNr">2008</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2007" name="year" class="pbtpt4-1 kBNAEq" value="2007">
									<label for="year-2007" class="pbtpt4-0 fcerNr">2007</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2006" name="year" class="pbtpt4-1 kBNAEq" value="2006">
									<label for="year-2006" class="pbtpt4-0 fcerNr">2006</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2005" name="year" class="pbtpt4-1 kBNAEq" value="2005">
									<label for="year-2005" class="pbtpt4-0 fcerNr">2005</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2004" name="year" class="pbtpt4-1 kBNAEq" value="2004">
									<label for="year-2004" class="pbtpt4-0 fcerNr">2004</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2003" name="year" class="pbtpt4-1 kBNAEq" value="2003">
									<label for="year-2003" class="pbtpt4-0 fcerNr">2003</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2002" name="year" class="pbtpt4-1 kBNAEq" value="2002">
									<label for="year-2002" class="pbtpt4-0 fcerNr">2002</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2001" name="year" class="pbtpt4-1 kBNAEq" value="2001">
									<label for="year-2001" class="pbtpt4-0 fcerNr">2001</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2000" name="year" class="pbtpt4-1 kBNAEq" value="2000">
									<label for="year-2000" class="pbtpt4-0 fcerNr">2000</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1999" name="year" class="pbtpt4-1 kBNAEq" value="1999">
									<label for="year-1999" class="pbtpt4-0 fcerNr">1999</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1998" name="year" class="pbtpt4-1 kBNAEq" value="1998">
									<label for="year-1998" class="pbtpt4-0 fcerNr">1998</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1997" name="year" class="pbtpt4-1 kBNAEq" value="1997">
									<label for="year-1997" class="pbtpt4-0 fcerNr">1997</label>
								</div>
							</div>
						</div>
						<button type="button" id="step2_submit" data-val="2" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step3 stepmain" step-val="3">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is your vehicle make?</h1>
						</div>
						<div class="kho8nb-0 kiSTuj">
							<input type="radio" id="make-BUICK" name="make" class="pbtpt4-1 kBNAEq" value="BUICK">
							<label for="make-BUICK" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 bGLRPF"></span></span>Buick</label>
							<input type="radio" id="make-CHEVROLET" name="make" class="pbtpt4-1 kBNAEq" value="CHEVROLET">
							<label for="make-CHEVROLET" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 kdfnAn"></span></span>Chevrolet</label>
							<input type="radio" id="make-CHRYSLER" name="make" class="pbtpt4-1 kBNAEq" value="CHRYSLER">
							<label for="make-CHRYSLER" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 kQOzIx"></span></span>Chrysler</label>
							<input type="radio" id="make-DODGE" name="make" class="pbtpt4-1 kBNAEq" value="DODGE">
							<label for="make-DODGE" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 dHsztc"></span></span>Dodge</label>
							<input type="radio" id="make-FORD" name="make" class="pbtpt4-1 kBNAEq" value="FORD">
							<label for="make-FORD" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 hyoHdA"></span></span>Ford</label>
							<input type="radio" id="make-GMC" name="make" class="pbtpt4-1 kBNAEq" value="GMC">
							<label for="make-GMC" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 hoxONX"></span></span>GMC</label>
							<input type="radio" id="make-HONDA" name="make" class="pbtpt4-1 kBNAEq" value="HONDA">
							<label for="make-HONDA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cAHhgw"></span></span>Honda</label>
							<input type="radio" id="make-HYUNDAI" name="make" class="pbtpt4-1 kBNAEq" value="HYUNDAI">
							<label for="make-HYUNDAI" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cIROEU"></span></span>Hyundai</label>
							<input type="radio" id="make-JEEP" name="make" class="pbtpt4-1 kBNAEq" value="JEEP">
							<label for="make-JEEP" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cYGTqO"></span></span>Jeep</label>
							<input type="radio" id="make-KIA" name="make" class="pbtpt4-1 kBNAEq" value="KIA">
							<label for="make-KIA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 gpCvTr"></span></span>Kia</label>
							<input type="radio" id="make-NISSAN" name="make" class="pbtpt4-1 kBNAEq" value="NISSAN">
							<label for="make-NISSAN" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 bCRADi"></span></span>Nissan</label>
							<input type="radio" id="make-TOYOTA" name="make" class="pbtpt4-1 kBNAEq" value="TOYOTA">
							<label for="make-TOYOTA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 jqEqHg"></span></span>Toyota</label>
						</div>
						<button type="button" id="step3_submit" data-val="3" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step4 stepmain" step-val="4">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the model of your KIA?</h1>
						</div>
						<div class="kia kho8nb-0 jDiNWS">
							<input type="radio" id="model-SEPHIA" name="model" class="pbtpt4-1 kBNAEq" value="SEPHIA">
							<label for="model-SEPHIA" class="pbtpt4-0 fcerNr">SEPHIA</label>
							<input type="radio" id="model-SPORTAGE" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-SPORTAGE" class="pbtpt4-0 fcerNr">SPORTAGE</label>
						</div>
						<div class="buick kho8nb-0 jDiNWS">
							<input type="radio" id="model-CENTURY" name="model" class="pbtpt4-1 kBNAEq" value="CENTURY">
							<label for="model-CENTURY" class="pbtpt4-0 fcerNr">CENTURY</label>
							<input type="radio" id="model-LESABRE" name="model" class="pbtpt4-1 kBNAEq" value="LESABRE">
							<label for="model-LESABRE" class="pbtpt4-0 fcerNr">LESABRE</label>
							<input type="radio" id="model-PARK-AVENUE" name="model" class="pbtpt4-1 kBNAEq" value="PARK AVENUE">
							<label for="model-PARK-AVENUE" class="pbtpt4-0 fcerNr">PARK AVENUE</label>
							<input type="radio" id="model-REGAL" name="model" class="pbtpt4-1 kBNAEq" value="REGAL">
							<label for="model-REGAL" class="pbtpt4-0 fcerNr">REGAL</label>
							<input type="radio" id="model-RIVIERA" name="model" class="pbtpt4-1 kBNAEq" value="RIVIERA">
							<label for="model-RIVIERA" class="pbtpt4-0 fcerNr">RIVIERA</label>
							<input type="radio" id="model-SKYLARK" name="model" class="pbtpt4-1 kBNAEq" value="SKYLARK">
							<label for="model-SKYLARK" class="pbtpt4-0 fcerNr">SKYLARK</label>
						</div>
						<button type="button" id="step4_submit" data-val="4" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step5 stepmain" step-val="5">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the trim on your KIA?</h1>
						</div>
						<div class="kia kho8nb-0 jDiNWS">
							<input type="radio" id="2wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SEPHIA">
							<label for="model-2wd-sedan" class="pbtpt4-0 fcerNr">2WD SEDAN - 6 CYL 3.8 L</label>
							<input type="radio" id="4wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-4wd-sedan" class="pbtpt4-0 fcerNr">4WD SEDAN - 6 CYL 3.8 L</label>
						</div>
						<div class="buick kho8nb-0 jDiNWS">
							<input type="radio" id="4wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-4wd-sedan8-cyl" class="pbtpt4-0 fcerNr">4WD SEDAN - 8 CYL 3.8 L</label>
						</div>
						<button type="button" id="step5_submit" data-val="5" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step6 stepmain" step-val="6">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">Do you own or lease your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="ownership-OWN-OWNED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_OWNED">
							<label for="ownership-OWN-OWNED" class="pbtpt4-0 eSQqD fcerNr">Owned</label>
							<input type="radio" id="ownership-OWN-FINANCED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_FINANCED">
							<label for="ownership-OWN-FINANCED" class="pbtpt4-0 eSQqD fcerNr">Financed</label>
							<input type="radio" id="ownership-OWN-LEASED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_LEASED">
							<label for="ownership-OWN-LEASED" class="pbtpt4-0 eSQqD fcerNr">Leased</label>
						</div>
						<button type="button" id="step6_submit" data-val="6" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step7 stepmain" step-val="7">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the primary use for your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="primaryUse-USE-COMMUTE" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_COMMUTE">
							<label for="primaryUse-USE-COMMUTE" class="pbtpt4-0 eSQqD fcerNr">Commute</label>
							<input type="radio" id="primaryUse-USE-PLEASURE" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_PLEASURE">
							<label for="primaryUse-USE-PLEASURE" class="pbtpt4-0 eSQqD fcerNr">Pleasure</label>
							<input type="radio" id="primaryUse-USE-BUSINESS" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_BUSINESS">
							<label for="primaryUse-USE-BUSINESS" class="pbtpt4-0 eSQqD fcerNr">Business</label>
							<input type="radio" id="primaryUse-USE-FARM" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_FARM">
							<label for="primaryUse-USE-FARM" class="pbtpt4-0 eSQqD fcerNr">Farm</label>
						</div>
						<button type="button" id="step7_submit" data-val="7" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step8 stepmain" step-val="8">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the annual mileage on your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="milesPerYear-5000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="5000">
							<label for="milesPerYear-5000" class="pbtpt4-0 fcerNr">Less than 5,000</label>
							<input type="radio" id="milesPerYear-10000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="10000">
							<label for="milesPerYear-10000" class="pbtpt4-0 fcerNr">5,000–10,000</label>
							<input type="radio" id="milesPerYear-15000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="15000">
							<label for="milesPerYear-15000" class="pbtpt4-0  fcerNr">10,000–15,000</label>
							<input type="radio" id="milesPerYear-20000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="20000">
							<label for="milesPerYear-20000" class="pbtpt4-0 fcerNr">15,000–20,000</label>
							<input type="radio" id="milesPerYear-50000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="50000">
							<label for="milesPerYear-50000" class="pbtpt4-0 fcerNr">More than 20,000</label>
						</div>
						<button type="button" id="step8_submit" data-val="8" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step9 stepmain" step-val="9">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">How much coverage do you need?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="coverageType-CT-ML" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_ML">
							<label for="coverageType-CT-ML" class="pbtpt4-0 fcerNr">State Minimum</label>
							<input type="radio" id="coverageType-CT-LL" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_LL">
							<label for="coverageType-CT-LL" class="pbtpt4-0 fcerNr">Lower Level</label>
							<input type="radio" id="coverageType-CT-FC" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_FC">
							<label for="coverageType-CT-FC" class="pbtpt4-0 fcerNr">Typical Level</label>
							<input type="radio" id="coverageType-CT-EFC" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_EFC">
							<label for="coverageType-CT-EFC" class="pbtpt4-0 fcerNr">Highest Level</label>
						</div>
						<button type="button" id="step9_submit" data-val="9" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step10 stepmain" step-val="10">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">Would you like to add a second vehicle?</h1>
						</div>
						<div class="kho8nb-0 dkXNvz">
							<input type="radio" id="addAuto-true" name="addAuto" class="pbtpt4-1 kBNAEq" value="true">
							<label for="addAuto-true" class="pbtpt4-0 fcerNr">Yes</label>
							<input type="radio" id="addAuto-false" name="addAuto" class="pbtpt4-1 kBNAEq" value="false">
							<label for="addAuto-false" class="pbtpt4-0 fcerNr">No</label>
						</div>
						<button type="button" id="step10_submit" data-val="10" class="btn btn-success">Continue</button>					
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step11 stepmain" step-val="11">
			<div class="container">
				<div class="row">
					<div class="col col-sm-3 ">
				  	</div>
					<div class="col col-sm-6">
						<div class="form-group">
					  	<label for="usr">What is your ZIP Code?</label>
					  	<input type="text" class="form-control" id="zipcode">
						</div>
						<button type="button" id="step11_submit" data-val="11" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-3 ">
						</div>
				</div>
			</div>
		</div>
		<div class="step12 stepmain" step-val="12">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="form-group">
							<div class="row">
								<h1 style="text-align:center;">What is your vehicle year?</h1>
								<div class="col col-sm-2">
									<input type="radio" id="year-2022" name="year" class="pbtpt4-1 kBNAEq" value="2022">
									<label for="year-2022" class="pbtpt4-0 fcerNr">2022</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2021" name="year" class="pbtpt4-1 kBNAEq" value="2021">
									<label for="year-2021" class="pbtpt4-0 fcerNr">2021</label>
							  </div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2020" name="year" class="pbtpt4-1 kBNAEq" value="2020">
									<label for="year-2020" class="pbtpt4-0 fcerNr">2020</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2019" name="year" class="pbtpt4-1 kBNAEq" value="2019">
									<label for="year-2019" class="pbtpt4-0 fcerNr">2019</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2018" name="year" class="pbtpt4-1 kBNAEq" value="2018">
									<label for="year-2018" class="pbtpt4-0 fcerNr">2018</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2017" name="year" class="pbtpt4-1 kBNAEq" value="2017">
									<label for="year-2017" class="pbtpt4-0 fcerNr">2017</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2016" name="year" class="pbtpt4-1 kBNAEq" value="2016">
									<label for="year-2016" class="pbtpt4-0 fcerNr">2016</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2015" name="year" class="pbtpt4-1 kBNAEq" value="2015">
									<label for="year-2015" class="pbtpt4-0 fcerNr">2015</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2014" name="year" class="pbtpt4-1 kBNAEq" value="2014">
									<label for="year-2014" class="pbtpt4-0 fcerNr">2014</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2013" name="year" class="pbtpt4-1 kBNAEq" value="2013">
									<label for="year-2013" class="pbtpt4-0 fcerNr">2013</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2012" name="year" class="pbtpt4-1 kBNAEq" value="2012">
									<label for="year-2012" class="pbtpt4-0 fcerNr">2012</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2011" name="year" class="pbtpt4-1 kBNAEq" value="2011">
									<label for="year-2011" class="pbtpt4-0 fcerNr">2011</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2010" name="year" class="pbtpt4-1 kBNAEq" value="2010">
									<label for="year-2010" class="pbtpt4-0 fcerNr">2010</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2009" name="year" class="pbtpt4-1 kBNAEq" value="2009">
									<label for="year-2009" class="pbtpt4-0 fcerNr">2009</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2008" name="year" class="pbtpt4-1 kBNAEq" value="2008">
									<label for="year-2008" class="pbtpt4-0 fcerNr">2008</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2007" name="year" class="pbtpt4-1 kBNAEq" value="2007">
									<label for="year-2007" class="pbtpt4-0 fcerNr">2007</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2006" name="year" class="pbtpt4-1 kBNAEq" value="2006">
									<label for="year-2006" class="pbtpt4-0 fcerNr">2006</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2005" name="year" class="pbtpt4-1 kBNAEq" value="2005">
									<label for="year-2005" class="pbtpt4-0 fcerNr">2005</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2004" name="year" class="pbtpt4-1 kBNAEq" value="2004">
									<label for="year-2004" class="pbtpt4-0 fcerNr">2004</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2003" name="year" class="pbtpt4-1 kBNAEq" value="2003">
									<label for="year-2003" class="pbtpt4-0 fcerNr">2003</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2002" name="year" class="pbtpt4-1 kBNAEq" value="2002">
									<label for="year-2002" class="pbtpt4-0 fcerNr">2002</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2001" name="year" class="pbtpt4-1 kBNAEq" value="2001">
									<label for="year-2001" class="pbtpt4-0 fcerNr">2001</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-2000" name="year" class="pbtpt4-1 kBNAEq" value="2000">
									<label for="year-2000" class="pbtpt4-0 fcerNr">2000</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1999" name="year" class="pbtpt4-1 kBNAEq" value="1999">
									<label for="year-1999" class="pbtpt4-0 fcerNr">1999</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1998" name="year" class="pbtpt4-1 kBNAEq" value="1998">
									<label for="year-1998" class="pbtpt4-0 fcerNr">1998</label>
								</div>
								<div class="col col-sm-2">
									<input type="radio" id="year-1997" name="year" class="pbtpt4-1 kBNAEq" value="1997">
									<label for="year-1997" class="pbtpt4-0 fcerNr">1997</label>
								</div>
							</div>
						</div>
						<button type="button" id="step12_submit" data-val="12" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step13 stepmain" step-val="13">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is your vehicle make?</h1>
						</div>
						<div class="kho8nb-0 kiSTuj">
							<input type="radio" id="make-BUICK" name="make" class="pbtpt4-1 kBNAEq" value="BUICK">
							<label for="make-BUICK" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 bGLRPF"></span></span>Buick</label>
							<input type="radio" id="make-CHEVROLET" name="make" class="pbtpt4-1 kBNAEq" value="CHEVROLET">
							<label for="make-CHEVROLET" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 kdfnAn"></span></span>Chevrolet</label>
							<input type="radio" id="make-CHRYSLER" name="make" class="pbtpt4-1 kBNAEq" value="CHRYSLER">
							<label for="make-CHRYSLER" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 kQOzIx"></span></span>Chrysler</label>
							<input type="radio" id="make-DODGE" name="make" class="pbtpt4-1 kBNAEq" value="DODGE">
							<label for="make-DODGE" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 dHsztc"></span></span>Dodge</label>
							<input type="radio" id="make-FORD" name="make" class="pbtpt4-1 kBNAEq" value="FORD">
							<label for="make-FORD" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 hyoHdA"></span></span>Ford</label>
							<input type="radio" id="make-GMC" name="make" class="pbtpt4-1 kBNAEq" value="GMC">
							<label for="make-GMC" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 hoxONX"></span></span>GMC</label>
							<input type="radio" id="make-HONDA" name="make" class="pbtpt4-1 kBNAEq" value="HONDA">
							<label for="make-HONDA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cAHhgw"></span></span>Honda</label>
							<input type="radio" id="make-HYUNDAI" name="make" class="pbtpt4-1 kBNAEq" value="HYUNDAI">
							<label for="make-HYUNDAI" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cIROEU"></span></span>Hyundai</label>
							<input type="radio" id="make-JEEP" name="make" class="pbtpt4-1 kBNAEq" value="JEEP">
							<label for="make-JEEP" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 cYGTqO"></span></span>Jeep</label>
							<input type="radio" id="make-KIA" name="make" class="pbtpt4-1 kBNAEq" value="KIA">
							<label for="make-KIA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 gpCvTr"></span></span>Kia</label>
							<input type="radio" id="make-NISSAN" name="make" class="pbtpt4-1 kBNAEq" value="NISSAN">
							<label for="make-NISSAN" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 bCRADi"></span></span>Nissan</label>
							<input type="radio" id="make-TOYOTA" name="make" class="pbtpt4-1 kBNAEq" value="TOYOTA">
							<label for="make-TOYOTA" class="pbtpt4-0 fcerNr"><span class="sc-18vx9w9-0 jFQjOZ"><span class="sc-18vx9w9-1 jqEqHg"></span></span>Toyota</label>
						</div>
						<button type="button" id="step13_submit" data-val="13" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step14 stepmain" step-val="14">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the model of your KIA?</h1>
						</div>
						<div class="kia kho8nb-0 jDiNWS">
							<input type="radio" id="model-SEPHIA" name="model" class="pbtpt4-1 kBNAEq" value="SEPHIA">
							<label for="model-SEPHIA" class="pbtpt4-0 fcerNr">SEPHIA</label>
							<input type="radio" id="model-SPORTAGE" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-SPORTAGE" class="pbtpt4-0 fcerNr">SPORTAGE</label>
						</div>
						<div class="buick kho8nb-0 jDiNWS">
							<input type="radio" id="model-CENTURY" name="model" class="pbtpt4-1 kBNAEq" value="CENTURY">
							<label for="model-CENTURY" class="pbtpt4-0 fcerNr">CENTURY</label>
							<input type="radio" id="model-LESABRE" name="model" class="pbtpt4-1 kBNAEq" value="LESABRE">
							<label for="model-LESABRE" class="pbtpt4-0 fcerNr">LESABRE</label>
							<input type="radio" id="model-PARK-AVENUE" name="model" class="pbtpt4-1 kBNAEq" value="PARK AVENUE">
							<label for="model-PARK-AVENUE" class="pbtpt4-0 fcerNr">PARK AVENUE</label>
							<input type="radio" id="model-REGAL" name="model" class="pbtpt4-1 kBNAEq" value="REGAL">
							<label for="model-REGAL" class="pbtpt4-0 fcerNr">REGAL</label>
							<input type="radio" id="model-RIVIERA" name="model" class="pbtpt4-1 kBNAEq" value="RIVIERA">
							<label for="model-RIVIERA" class="pbtpt4-0 fcerNr">RIVIERA</label>
							<input type="radio" id="model-SKYLARK" name="model" class="pbtpt4-1 kBNAEq" value="SKYLARK">
							<label for="model-SKYLARK" class="pbtpt4-0 fcerNr">SKYLARK</label>
						</div>
						<button type="button" id="step14_submit" data-val="14" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step15 stepmain" step-val="15">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the trim on your KIA?</h1>
						</div>
						<div class="kia kho8nb-0 jDiNWS">
							<input type="radio" id="2wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SEPHIA">
							<label for="model-2wd-sedan" class="pbtpt4-0 fcerNr">2WD SEDAN - 6 CYL 3.8 L</label>
							<input type="radio" id="4wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-4wd-sedan" class="pbtpt4-0 fcerNr">4WD SEDAN - 6 CYL 3.8 L</label>
						</div>
						<div class="buick kho8nb-0 jDiNWS">
							<input type="radio" id="4wd-sedan" name="model" class="pbtpt4-1 kBNAEq" value="SPORTAGE">
							<label for="model-4wd-sedan8-cyl" class="pbtpt4-0 fcerNr">4WD SEDAN - 8 CYL 3.8 L</label>
						</div>
						<button type="button" id="step14_submit" data-val="15" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step16 stepmain" step-val="16">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">Do you own or lease your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="ownership-OWN-OWNED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_OWNED">
							<label for="ownership-OWN-OWNED" class="pbtpt4-0 eSQqD fcerNr">Owned</label>
							<input type="radio" id="ownership-OWN-FINANCED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_FINANCED">
							<label for="ownership-OWN-FINANCED" class="pbtpt4-0 eSQqD fcerNr">Financed</label>
							<input type="radio" id="ownership-OWN-LEASED" name="ownership" class="pbtpt4-1 kBNAEq" value="OWN_LEASED">
							<label for="ownership-OWN-LEASED" class="pbtpt4-0 eSQqD fcerNr">Leased</label>
						</div>
						<button type="button" id="step16_submit" data-val="16" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step17 stepmain" step-val="17">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the primary use for your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="primaryUse-USE-COMMUTE" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_COMMUTE">
							<label for="primaryUse-USE-COMMUTE" class="pbtpt4-0 eSQqD fcerNr">Commute</label>
							<input type="radio" id="primaryUse-USE-PLEASURE" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_PLEASURE">
							<label for="primaryUse-USE-PLEASURE" class="pbtpt4-0 eSQqD fcerNr">Pleasure</label>
							<input type="radio" id="primaryUse-USE-BUSINESS" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_BUSINESS">
							<label for="primaryUse-USE-BUSINESS" class="pbtpt4-0 eSQqD fcerNr">Business</label>
							<input type="radio" id="primaryUse-USE-FARM" name="primaryUse" class="pbtpt4-1 kBNAEq" value="USE_FARM">
							<label for="primaryUse-USE-FARM" class="pbtpt4-0 eSQqD fcerNr">Farm</label>
						</div>
						<button type="button" id="step17_submit" data-val="17" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step18 stepmain" step-val="18">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">What is the annual mileage on your KIA?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="milesPerYear-5000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="5000">
							<label for="milesPerYear-5000" class="pbtpt4-0 fcerNr">Less than 5,000</label>
							<input type="radio" id="milesPerYear-10000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="10000">
							<label for="milesPerYear-10000" class="pbtpt4-0 fcerNr">5,000–10,000</label>
							<input type="radio" id="milesPerYear-15000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="15000">
							<label for="milesPerYear-15000" class="pbtpt4-0  fcerNr">10,000–15,000</label>
							<input type="radio" id="milesPerYear-20000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="20000">
							<label for="milesPerYear-20000" class="pbtpt4-0 fcerNr">15,000–20,000</label>
							<input type="radio" id="milesPerYear-50000" name="milesPerYear" class="pbtpt4-1 kBNAEq" value="50000">
							<label for="milesPerYear-50000" class="pbtpt4-0 fcerNr">More than 20,000</label>
						</div>
						<button type="button" id="step18_submit" data-val="18" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step19 stepmain" step-val="19">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">How much coverage do you need?</h1>
						</div>
						<div class="kho8nb-0 jDiNWS">
							<input type="radio" id="coverageType-CT-ML" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_ML">
							<label for="coverageType-CT-ML" class="pbtpt4-0 fcerNr">State Minimum</label>
							<input type="radio" id="coverageType-CT-LL" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_LL">
							<label for="coverageType-CT-LL" class="pbtpt4-0 fcerNr">Lower Level</label>
							<input type="radio" id="coverageType-CT-FC" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_FC">
							<label for="coverageType-CT-FC" class="pbtpt4-0 fcerNr">Typical Level</label>
							<input type="radio" id="coverageType-CT-EFC" name="coverageType" class="pbtpt4-1 kBNAEq" value="CT_EFC">
							<label for="coverageType-CT-EFC" class="pbtpt4-0 fcerNr">Highest Level</label>
						</div>
						<button type="button" id="step19_submit" data-val="19" class="btn btn-success">Continue</button>
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="step20 stepmain" step-val="20">
			<div class="container">
				<div class="row">
					<div class="col col-sm-2"></div>
					<div class="col col-sm-8">
						<div class="row">
							<h1 style="text-align:center;">Would you like to add a second vehicle?</h1>
						</div>
						<div class="kho8nb-0 dkXNvz">
							<input type="radio" id="addAuto-true" name="addAuto" class="pbtpt4-1 kBNAEq" value="true">
							<label for="addAuto-true" class="pbtpt4-0 fcerNr">Yes</label>
							<input type="radio" id="addAuto-false" name="addAuto" class="pbtpt4-1 kBNAEq" value="false">
							<label for="addAuto-false" class="pbtpt4-0 fcerNr">No</label>
						</div>
						<button type="button" id="step20_submit" data-val="20" class="btn btn-success">Continue</button>					
					</div>
					<div class="col col-sm-2"></div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	jQuery( document ).ready(function() {

		//Hide all steps be default
		//Loop on each steps
		jQuery( ".stepmain" ).each(function( index ) {
			var firststep = 1;
			jQuery(this).hide();
			var current_index = jQuery(this).attr('step-val');
			if (current_index == 1){
		  		jQuery(this).show();
		  	}
		});

	    jQuery(".stepmain .btn.btn-success").click(function(){
		  var current_step = jQuery(this).attr('data-val');
		  var next_step = parseInt(current_step) + parseInt(1);
		  
		  //Loop on each steps
		  jQuery( ".stepmain" ).each(function( index ) {
		  	jQuery(this).hide();

		  	var current_index = jQuery(this).attr('step-val');
		  	if (current_index == next_step){
		  		jQuery(this).show();
		  	}

		  });

		});
	});
</script>




<?php

    // return the buffer contents and delete
    return ob_get_clean();
}

// Now we set that function up to execute when the admin_notices action is called.
add_shortcode( 'insform', 'insuranceForm' );
