// New Angular code
jQuery(document).ready(function() {
	(function() {

	jQuery("#remove-color-distance").slider({
		range: "min",
		value: 0.82,
		min: 0,
		max: 1,
		step: 0.01,
		animate: 200,
		slide: function(event, ui) {
			console.log("Remove Color Distance"+ui.value);
			applyFilterValue(2, 'distance', ui.value);
		}
	}); 

	//Get Object From Canvas By Id
	function getObjectFromCanvasById(id) {
	    const canvasObject = canvas.getObjects().filter((item) => {
	        return item.id === parseInt(id)
	    })
	    //return canvasObject[0]
	}
	//Remove Object From Canvas By ID
	function removeObjectFromCanvas(objectId) {
	    const canvasObject = getObjectFromCanvasById(objectId)
	    canvas.remove(canvasObject)
	}
	function degToRad(degrees) {
		return degrees * (Math.PI / 180);
	}
	function applyFilter(index, filter) {
		var obj = canvas.getActiveObject();
		obj.filters[index] = filter;
		var timeStart = +new Date();
		obj.applyFilters();
		var timeEnd = +new Date();
		var dimString = canvas.getActiveObject().width + ' x ' +
		canvas.getActiveObject().height;
		$('bench').innerHTML = dimString + 'px ' +
		parseFloat(timeEnd-timeStart) + 'ms';
		canvas.renderAll();
	}

	function getFilter(index) {
		var obj = canvas.getActiveObject();
		return obj.filters[index];
	}

	function applyFilterValue(index, prop, value) {
		var obj = canvas.getActiveObject();
		if (obj.filters[index]) {
			obj.filters[index][prop] = value;
			var timeStart = +new Date();
			obj.applyFilters();
			var timeEnd = +new Date();
			var dimString = canvas.getActiveObject().width + ' x ' +
			canvas.getActiveObject().height;
			jQuery('bench').innerHTML = dimString + 'px ' +
			parseFloat(timeEnd-timeStart) + 'ms';
			canvas.renderAll();
		}
	}

	fabric.Object.prototype.padding = 5;
	fabric.Object.prototype.transparentCorners = false;

	//Get views Background
	var side_a_image = document.getElementById('side_a_image').value;
	var side_b_image = document.getElementById('side_b_image').value;
	var side_a_bg_top = document.getElementById('side_a_bg_top').value;
	var side_a_bg_left = document.getElementById('side_a_bg_left').value;
	var side_a_bg_scale = document.getElementById('side_a_bg_scale').value;
	var side_b_bg_top = document.getElementById('side_b_bg_top').value;
	var side_b_bg_left = document.getElementById('side_b_bg_left').value;
	var side_b_bg_scale = document.getElementById('side_b_bg_scale').value;
	var canvas_width = document.getElementById('canvas_width').value;
	var canvas_height = document.getElementById('canvas_height').value;

	//Canvas Switching
	var canvas1 = new fabric.Canvas('canvas1');
	canvas1.setDimensions({width:canvas_width, height:canvas_height});
	var canvas2 = new fabric.Canvas('canvas2');
	canvas2.setDimensions({width:canvas_width, height:canvas_height});

	BoundingBoxCanvas1();
	BoundingBoxCanvas2();

	fabric.Image.fromURL(side_a_image, function(img) {
		var side_a_image = img.set({ 
			id: 'canvas1bg'
			});
	});	
	fabric.Image.fromURL(side_b_image, function(img) {
		var side_b_image = img.set({ 
			id: 'canvas2bg',
			});
	});
	canvas1.setBackgroundImage(side_a_image, canvas1.renderAll.bind(canvas1),{
		top: parseInt(side_a_bg_top),
		left: parseInt(side_a_bg_left), 
 
	});
	canvas2.setBackgroundImage(side_b_image, canvas2.renderAll.bind(canvas2),{
		top: parseInt(side_b_bg_top),
		left: parseInt(side_b_bg_left), 
	});

  	f = fabric.Image.filters;
	
	var switcher = 1;

	if(switcher == 1) {
	    var canvas = canvas1;
	    jQuery("#canvas2").parent().hide();
	    jQuery("#canvas2").parent().hide();
	        fireEvents();
	}

	jQuery('.select-side-inner input:radio').click(function() {
	if (jQuery(this).val() === 'sidea') { 
	  	switcher = 1;
	    jQuery("#canvas1").parent().show()
	    jQuery("#canvas2").parent().hide();
	    
	    canvas = canvas1;
	    fireEvents();
	    //canvas.deactivateAllWithDispatch();			    
	    canvas.calcOffset();
	} 
	else if (jQuery(this).val() === 'sideb') {
		switcher = 2;
	    jQuery("#canvas1").parent().hide()
	    jQuery("#canvas2").parent().show();
	    
	    canvas = canvas2;
	    fireEvents();
	    //canvas.deactivateAllWithDispatch();
	    canvas.calcOffset();
	} 
	});

	function fireEvents(){
		canvas.on('object:selected', function(options){
		 // Event actions
		});
		//removeObjectFromCanvas('canvasbg');
	}
	

	canvas.on({
	  	'selection:created': function() {
	      //fabric.util.toArray(document.getElementsByTagName('input')).forEach(function(el){ el.disabled = false; })

	      if(canvas.getActiveObject().get('type')==="Image"){
	      	var filters = ['grayscale', 'invert', 'remove-color', 'sepia', 'brownie',
	      	'brightness', 'contrast', 'saturation', 'noise', 'vintage',
	      	'pixelate', 'blur', 'sharpen', 'emboss', 'technicolor',
	      	'polaroid', 'blend-color', 'gamma', 'kodachrome',
	      	'blackwhite', 'blend-image', 'hue', 'resize'];

	      	for (var i = 0; i < filters.length; i++) {
	      		jQuery(filters[i]) && (
	      			jQuery(filters[i]).checked = !!canvas.getActiveObject().filters[i]);
	      	}
	      }

	  },
	  'selection:cleared': function() {
	      //fabric.util.toArray(document.getElementsByTagName('input')).forEach(function(el){ el.disabled = true; })
	  }
	});

	function adjustTextWidth(evt) {
	  if (evt.target instanceof fabric.IText) {
	    const text = evt.target.text || ""
	    while (evt.target.textLines.length > text.split("\n").length) {
	      evt.target.set({width: evt.target.getScaledWidth() + 1});
	    }
	  }
	}
	canvas.on("text:changed", adjustTextWidth);

 	// Custom Control
 	fabric.Canvas.prototype.customiseControls({
 		settings: {
 			borderColor: '#fe6c6a',
 			cornerSize: 20,
 			cornerShape: 'rect',
 			cornerBackgroundColor: '#fe6c6a',
 			cornerPadding: 5
 		},
 		tl: {
 			action: function( e, object ) {
 				var object = fabric.util.object.clone(canvas.getActiveObject());
 				object.set("top", object.top+5);
 				object.set("left", object.left+5);
 				canvas.add(object);
 			}
 		},
 		tr: {
 			action: 'rotate',
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/rotate.svg'
 		},
 		bl: {
 			action: function( e, object ) {
 				var obj = canvas.getActiveObject();
 				console.log("obj.id.."+obj.id);
 				var removedobjid = obj.id;
 				jQuery('#'+obj.id).remove();
 				var current_id = 'cardalltexthex1';
 				canvas.remove(obj);
 			},
 			cursor: 'pointer',
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/removew.svg'
 		},
 		br: {
 			action: 'scale',
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/resize.svg'
 		},
 		mb: false,
	    // only is hasRotatingPoint is not set to false
	    mtr: false,
	}, function() {
		canvas.renderAll();
	});
 	fabric.Object.prototype.customiseCornerIcons({
 		settings: {
 			borderColor: '#fe6c6a',
 			cornerSize: 20,
 			cornerShape: 'rect',
 			cornerBackgroundColor: '#fe6c6a',
 			cornerPadding: 5
 		},
 		tl: {
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/clone.svg'
 		},	    
 		tr: {
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/rotate.svg'
 		},
 		bl: {
 			action: 'remove',
 			cursor: 'pointer',
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/removew.svg'
 		},
 		br: {
 			icon: 'https://dev.kumailenterprises.com/wp-content/plugins/custom_fancy_product/images/icons/resize.svg'
 		},
 		mb: false,
	    // only is hasRotatingPoint is not set to false
	    mtr: false,
	}, function() {
		canvas.renderAll();
	});

 	function BoundingBoxCanvas1(){
		
		//Canvas 1
			var bounding_box_a_left = document.getElementById('bounding_box_a_left').value;
			var bounding_box_a_top = document.getElementById('bounding_box_a_top').value;
			var bounding_box_a_width = document.getElementById('bounding_box_a_width').value;
			var bounding_box_a_height = document.getElementById('bounding_box_a_height').value;

console.log(bounding_box_a_top);
console.log(bounding_box_a_left);

			//Bounding Box
			var boundingbox = new fabric.Rect({
				left: parseInt(bounding_box_a_left),
				top: parseInt(bounding_box_a_top),
				width: parseInt(bounding_box_a_width),
				height: parseInt(bounding_box_a_height),
				fill: '', /* use transparentCorners for no fill */
				stroke: '#f7f5f5',
				strokeWidth: 1,
				selectable: false,
				evented: false
			});	
			//canvas1.bringForward(boundingbox);
			canvas1.sendToBack(boundingbox);
			canvas1.add(boundingbox);
			canvas1.renderAll();
		return boundingbox;	
	}
	function BoundingBoxCanvas2(){
		
		//Canvas 1
			var bounding_box_b_left = document.getElementById('bounding_box_b_left').value;
			var bounding_box_b_top = document.getElementById('bounding_box_b_top').value;
			var bounding_box_b_width = document.getElementById('bounding_box_b_width').value;
			var bounding_box_b_height = document.getElementById('bounding_box_b_height').value;

			//Bounding Box
			var boundingbox = new fabric.Rect({
				left: parseInt(bounding_box_b_left),
				top: parseInt(bounding_box_b_top),
				width: parseInt(bounding_box_b_width),
				height: parseInt(bounding_box_b_height),
				fill: '', /* use transparentCorners for no fill */
				stroke: '#f7f5f5',
				strokeWidth: 1,
				selectable: false,
				evented: false
			});	
			//canvas2.bringForward(boundingbox);
			canvas2.sendToBack(boundingbox);
			canvas2.add(boundingbox);
			canvas2.renderAll();
		return boundingbox;	
	}

	//Add image to canvas
	// jQuery('.select_image').change(function (e) {
	// 	loadImage(e.target.files[0]);
	// });
	jQuery(".cat_back").hide();
	//Select Icon Category Click
	//Animals
	jQuery( ".icons-category-list .cat_parent .cat_icons div" ).click(function(e) {
		jQuery(".icons-list .cat_parent").hide();
		jQuery(".categoryicons,.cat_back").show();
		var selected_category = jQuery(this).attr("data-catname");
		jQuery( ".icons-category-inner" ).each(function( index ) {
			if(jQuery(this).attr("data-catname") == selected_category){
				jQuery(this).show();
			}else{
				jQuery(this).hide();
			}

		});
		
	});
	// //Popular
	// jQuery( ".icons-category-list .popular_cat.cat_parent div" ).click(function(e) {
	// 	jQuery(".popular_inner,.cat_back").show();
	// 	jQuery(".popular_cat .cat_icons,.animals_cat").hide();
	// });
	// //Celebrity
	// jQuery( ".icons-category-list .celebrity_cat.cat_parent div" ).click(function(e) {
	// 	jQuery(".celebrity_inner,.cat_back").show();
	// 	jQuery(".celebrity_cat .cat_icons,.animals_cat,.popular_cat").hide();
	// });
	//Go back Button Select Icon
	jQuery(document).on('click', '.cat_back', function () {
		jQuery(".categoryicons,.cat_back").hide();
		jQuery(".icons-list .cat_parent").show();
	});

	//Gallery Images Click function
	jQuery( ".icons-category-list .icons-list a" ).click(function(e) {
		//alert(jQuery(this).attr('data-val'));
		jQuery(".image_options").hide();

		e = e || window.event;
		e.preventDefault();
		e.stopPropagation();
		jQuery(".sl-wrapper.simple-lightbox,.sl-overlay").hide();

		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}
		var clipByName = function (ctx) {
			this.setCoords();
			var scaleXTo1 = (1 / this.scaleX);
			var scaleYTo1 = (1 / this.scaleY);
			ctx.save();

			var ctxLeft = -( this.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( this.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(this.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - this.oCoords.tl.x -5,
				boundingbox.top - this.oCoords.tl.y - 5,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}
		fabric.Image.fromURL(jQuery(this).attr('data-src'), function(img) {
			var logo = img.set({ 
				left: boundingbox.left + (boundingbox.width / 5), 
				top: boundingbox.top + (boundingbox.height / 5),
				id: "galleryicons",
				clipName: 'logo',
					clipTo: function(ctx) { 
					return _.bind(clipByName, logo)(ctx) 
				}
			});
			canvas.add(logo);			
		    logo.scaleToWidth(140);
		    logo.bringToFront();
		    logo.setControlsVisibility({
		    	bl: true,
		    	br: true,
		    	tl: true,
		    	tr: true,
		    	mb: false,
		    	ml: false,
		    	mr: false,
		    	mt: false,
		    	mtr: true,
		    });
		    logo.hasRotatingPoint = false;
			canvas.renderAll();
		});	

	});
	function loadImage(src) {
		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}
		var clipByName = function (ctx) {
			this.setCoords();
			var scaleXTo1 = (1 / this.scaleX);
			var scaleYTo1 = (1 / this.scaleY);
			ctx.save();

			var ctxLeft = -( this.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( this.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(this.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - this.oCoords.tl.x -5,
				boundingbox.top - this.oCoords.tl.y - 5,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}
		fabric.Image.fromURL(src, function(img) {
			var logo = img.set({ 
				left: boundingbox.left + (boundingbox.width / 5), 
				top: boundingbox.top + (boundingbox.height / 5),
				id: "galleryicons",
				clipName: 'logo',
					clipTo: function(ctx) { 
					return _.bind(clipByName, logo)(ctx) 
				}
			});
			canvas.add(logo);			
		    logo.scaleToWidth(140);
		    logo.bringToFront();
		    logo.setControlsVisibility({
		    	bl: true,
		    	br: true,
		    	tl: true,
		    	tr: true,
		    	mb: false,
		    	ml: false,
		    	mr: false,
		    	mt: false,
		    	mtr: true,
		    });
		    logo.hasRotatingPoint = false;
			canvas.renderAll();
		});	
	}
	function imageAdded(){
		//canvas.setActiveObject(canvas.item(0));
		var engraving_color_var = document.getElementById('engraving_color').value;
		console.log(engraving_color_var);
		//applyFilter(19, 'true' && new f.BlackWhite());
		//#fe6c6a

		applyFilter(2, 'true' && new f.RemoveColor({
			distance: 0.99,
		}));
		applyFilter(16, 'true' && new f.BlendColor({
			color: engraving_color_var,
      		mode: 'overlay',
      		alpha: 0.9
		}));
		
		applyFilterValue(2, 'distance', 0.6);
		applyFilterValue(16, 'color', engraving_color_var);
		//applyFilterValue(2, 'color', engraving_color_var);
		// applyFilter(6, 'true' && new f.Contrast({
		// 	contrast: 1,
		// }));
		applyFilter(5, 'true' && new f.Brightness({
			brightness: -1,
		}));
		canvas.renderAll();
	}  

	jQuery(".image_options").hide();
	jQuery(".text_options").hide();
		//Sence on canvas items click
		canvas.on('mouse:down', function(options) {
			if (options.target) {
				console.log('an object was clicked! ', options.target.type);	  

				if (options.target.type == "image"){
					jQuery(".image_options").show();
					jQuery(".text_options").hide();

					jQuery(".upload-image-content,.add-text-content,.select-icon-content").hide();
					if (options.target.id == "galleryicons"){
						jQuery(".image_options .contrast-control, .image_options #situation_reverse_main").hide();
						jQuery(".image_options").css("height","97px");
					}else{
						jQuery(".image_options").css("height","auto");
						jQuery(".image_options .contrast-control, .image_options #situation_reverse_main").show();
					}

				}
				else if (options.target.type == "textbox"){
					jQuery(".image_options").hide();
					jQuery(".text_options").show();
					jQuery(".upload-image-content,.add-text-content,.select-icon-content").hide();

				}
				else if (options.target.type == "text-curved"){
					jQuery(".image_options").hide();
					jQuery(".text_options").show();
					jQuery(".upload-image-content,.add-text-content,.select-icon-content").hide();

				}     
			} 
		});

	// Add Text code
	var count_text = 0;
	function Addtext() { 

	 //  	//Bounding Box
	 //  	var clipRectText = new fabric.Rect({
	 //  		originX: 'left',
	 //  		originY: 'top',
	 //  		left: 290,
	 //  		top: 145,
	 //  		width: 275,
	 //  		height: 290,
	 //  		fill: '', /* use transparentCorners for no fill */
	 //  		stroke: '#f7f5f5',
	 //  		strokeWidth: 1,
	 //  		cornerSize: 6,
	 //  		selectable: false,
	 //  	});
		// // find the one by which they want to be clipped.
		// clipRectText.set({
		//     clipFor: 'textcanvas'
		// });
		// canvas.add(clipRectText);
		// canvas.sendToBack(clipRectText);
		//Get Selected View
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();

		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();

		}
		function findByClipName(name) {
		    return _(canvas.getObjects()).where({
		            clipFor: name
		        }).first()
		}
		var clipByName = function (ctx) {
			this.setCoords();
			var scaleXTo1 = (1 / this.scaleX);
			var scaleYTo1 = (1 / this.scaleY);
			ctx.save();

			var ctxLeft = -( this.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( this.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(this.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - this.oCoords.tl.x -10,
				boundingbox.top - this.oCoords.tl.y - 10,
				boundingbox.width - 4,
				boundingbox.height - 4 
				);
			ctx.closePath();
			ctx.restore();
		}
		count_text = parseInt(count_text) + 1;
		var custom_text = jQuery("#add_text_input").val();
		var current_id = 'cardalltexthex'+count_text;
		console.log("current_id.."+current_id);
		jQuery("#canvas_added_texts").append("<input class='text_input' data-text='' value='"+custom_text+"' id='"+current_id+"'>"); 

		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}

		textcanvas = new fabric.Textbox(custom_text, { 
			id: current_id,
			left: boundingbox.left + (boundingbox.width / 2),
			top: boundingbox.top + (boundingbox.height / 2),
			textAlign: 'center',
			fontFamily: 'Nehama',
			fill: '#333',
			splitByGrapheme: true,
			fontSize: 30,
			padding: 10,
			resizeable: true
		});
		textcanvas.clipTo = function(ctx) { 
			return _.bind(clipByName, textcanvas)(ctx) 
		}
		textcanvas.set({width: textcanvas.getScaledWidth() + 1})

	 	canvas.add(textcanvas);
	 	canvas.setActiveObject(textcanvas);
	 	canvas.bringToFront(textcanvas);
	 	canvas.centerObject(textcanvas);
	 	textcanvas.setControlsVisibility({
	 		bl: true,
	 		br: true,
	 		tl: true,
	 		tr: true,
	 		mb: false,
	 		ml: false,
	 		mr: false,
	 		mt: false,
	 		mtr: false,
	 	});

	    //Text Align Icons
	    fabric.util.addListener(document.getElementById('text_icon_down'), 'click', function () {    
	    	var activeObj = canvas.getActiveObject();
			//canvas.setActiveObject(canvas.item(0));
			var obj = activeObj;
			obj.set({
				top: boundingbox.top + boundingbox.height - obj.height,
				selectable: true,
				hasBorders: false,
				hasRotatingPoint: false
			});
			canvas.renderAll();
		});
	    fabric.util.addListener(document.getElementById('text_icon_left'), 'click', function () {    
			//canvas.setActiveObject(canvas.item(0));
			var obj = canvas.getActiveObject();
			obj.set({
				left: boundingbox.left + 10,
				selectable: true,
				hasBorders: false,
				hasRotatingPoint: false
			});
			canvas.renderAll();
		});

	    fabric.util.addListener(document.getElementById('text_icon_up'), 'click', function () {    
			//canvas.setActiveObject(canvas.item(0));
			var obj = canvas.getActiveObject();
			obj.set({
				top: boundingbox.top,
				selectable: true,
				hasBorders: false,
				hasRotatingPoint: false
			});
			canvas.renderAll();
		});
	    fabric.util.addListener(document.getElementById('text_icon_right'), 'click', function () {    
			//canvas.setActiveObject(canvas.item(0));
			var obj = canvas.getActiveObject();
			obj.set({
				left: boundingbox.left + boundingbox.width - obj.getScaledWidth(),
				selectable: true,
				hasBorders: false,
				hasRotatingPoint: false
			});
			canvas.renderAll();
		});
	}	
		//Text Center Horizontal
		fabric.util.addListener(document.getElementById('text_center_h'), 'click', function () {    
			//canvas.setActiveObject(canvas.item(0));
			console.log("Flip X");
			var obj = canvas.getActiveObject();
			// obj.set({
			//       left: clipRectText.left + (clipRectText.width / 2) - (obj.getScaledWidth() / 2),
			//       selectable: true,
			//       hasBorders: false,
			//       hasRotatingPoint: true,
			//   });
			obj.toggle('flipX');

			//obj.rotate(180);
			canvas.renderAll();
		});
		//Text Center Vertical
		fabric.util.addListener(document.getElementById('text_center_v'), 'click', function () {    
			//canvas.setActiveObject(canvas.item(0));
			console.log("Flip Y");		
			var obj = canvas.getActiveObject();
			// obj.set({
			//       top: clipRectText.top + (clipRectText.height / 2) - (obj.getScaledHeight() / 2),
			//       selectable: true,
			//       hasBorders: false,
			//       hasRotatingPoint: true,
			//       flipY: true
			//   });
			obj.toggle('flipY');
			canvas.renderAll();
		});	
	

	var indexF;
	jQuery('webgl').onclick = function() {
		if (this.checked) {
			fabric.filterBackend = webglBackend;
		} else {
			fabric.filterBackend = canvas2dBackend;
		}
	};

  //Editable input fields
  //jQuery("#canvas_added_texts input").on('keyup', function() {
  //jQuery("#canvas_added_texts input").keyup(function() {
  	jQuery('body').on("keyup",'.text_input', function(){
  		console.log("key up");
  		id = jQuery(this).attr('id');
  		val = jQuery(this).attr('data-text');
  		newtext = jQuery(this).val();
  		input = jQuery(this);
  		objs = canvas.getObjects();
  		objs.forEach(function(obj) {

     //var curr_canvas_texts_id = obj.get('id');
    // jQuery("#canvas_added_texts").append("<input class='text_input' data-text='' value='' id='"+curr_canvas_texts_id+"' />"); 

    console.log(obj.get('id'));
    if (obj && obj.id == id) {
    	obj.set('text',newtext);
    	input.attr("data-text", newtext);
    	input.attr("val", newtext);
    	canvas.renderAll();
    }
});
  	});

  //jQuery("#blackwhite").click(function(){
    //console.log("Black White");

  //});
  // jQuery("#grayscale").click(function(){
  //   applyFilter(0, this.value && new f.Grayscale());
  // });
  invertsituation = document.getElementById("invert");
  invertsituation.onclick = function () { 
  	applyFilter(1, this.checked && new f.Invert());
  };
  //rcolor = document.getElementById("remove-color");
  // rcolor.onclick = function () {    
  //   console.log(this.checked);
  //   applyFilter(2, this.checked && new f.RemoveColor({
  //     distance: 0.82,
  //   }));
  // };
  
  // jQuery('#remove-color-color').onchange = function() {
  //   applyFilterValue(2, 'color', this.value);
  // };
  //jQuery('#remove-color-distance').oninput = function() {
  // jQuery('#remove-color-distance').on('input', distancevalueUpdated);  
  // function distancevalueUpdated (e) {
  //   var distanceval = jQuery(e.element).val();
  //   console.log("Remove Color Distance"+distanceval);
  //   applyFilterValue(2, 'distance', distanceval);
  // };
  // jQuery( "#remove-color-distance" ).on( "slidechange", function( event, ui ) {
  //   // use ui.value to get the current value

  // });
	//fabric.util.addListener(document.getElementById('uploaded-iamges-main'), 'click', function () {
	//jQuery(".uploaded_images div").click(function(){ 
	// window.addEventListener( "afu_got_response", function(e){
	//     var data = e.data; // full data object
	//     if( data.response.success ) { // success
	//         console.log( data.response.media_uri ); // the uploaded media URL
	//         jQuery("#uploaded_images").append("<img width='80px' src='"+data.response.media_uri+"'>");
	//     }
	// }, false);
	var retrieved_images = localStorage.getItem("user_images_content");
	jQuery( "#uploaded_images" ).html(retrieved_images);
	window.addEventListener( "afu_file_uploaded", function(e){
	    if( "undefined" !== typeof e.data.response.media_uri ) {
	        console.log( e.data.response.media_uri ); // the uploaded media URL
	        loadImage(e.data.response.media_uri);
	        jQuery("#uploaded_images").append("<div class='single-image'><span class='remove-image'>X</span><img width='80px' src='"+e.data.response.media_uri+"'></div>");
	        var added_images = jQuery( "#uploaded_images" ).html();
	        localStorage.setItem("user_images_content", added_images);
	    }
	}, false);	
	
	//Remove Image when clicked
	jQuery(document).on('click', '.single-image .remove-image', function() {
		jQuery(this).parent().remove();
	});

	//Images Uploaded Click Option
	jQuery(document).on('click', '.upload-image-content img', function() {
		console.log("img click...");
		
		//Get Selected View
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();

		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}		
 		var clipByName = function (ctx) {
			this.setCoords();
			var scaleXTo1 = (1 / this.scaleX);
			var scaleYTo1 = (1 / this.scaleY);
			ctx.save();

			var ctxLeft = -( this.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( this.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(this.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - this.oCoords.tl.x -5,
				boundingbox.top - this.oCoords.tl.y - 5,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}
		fabric.Image.fromURL(jQuery(this).attr('src'), function(img) {
			var logo = img.set({ 
				left: boundingbox.left + (boundingbox.width / 5), 
				top: boundingbox.top + (boundingbox.height / 5),
				clipName: 'logo',
					clipTo: function(ctx) { 
					return _.bind(clipByName, logo)(ctx) 
				}
			});
			//var engraving_color_var = document.getElementById('engraving_color').value;
			
			// var filter = new fabric.Image.filters.BlendColor({
			//  color: engraving_color_var,
			//  mode: 'overlay',
			//  opacity: 0.3
			// });
			// logo.filters.push(filter);
			// logo.applyFilters();
			
			canvas.add(logo);
			
			logo.setControlsVisibility({
		    	bl: true,
		    	br: true,
		    	tl: true,
		    	tr: true,
		    	mb: false,
		    	ml: false,
		    	mr: false,
		    	mt: false,
		    	mtr: true,
		    });
		    logo.scaleToWidth(140);
		    logo.hasRotatingPoint = false;
		    canvas.setActiveObject(logo);
			canvas.renderAll();
			var engraving_feature = jQuery("#engraving_feature").val();
			if (engraving_feature == "Enable") {
				imageAdded();
			}

		});
	
		fabric.util.addListener(document.getElementById('icon_right'), 'click', function () {    		
			//var boundingbox = localStorage.getItem("boundingbox");
		    var obj = canvas.getActiveObject();	    
		    console.log(obj.getScaledWidth());
		    obj.set({
		    	left: boundingbox.left + boundingbox.width - obj.getScaledWidth(),
		    	selectable: true,
		    	hasBorders: true,
		    	hasRotatingPoint: false
		    });
		    canvas.setActiveObject(obj);
		    canvas.renderAll();				
		});
	});
	fabric.util.addListener(document.getElementById('icon_down'), 'click', function () {    
	    //canvas.setActiveObject(canvas.item(0));
	    var obj = canvas.getActiveObject();
	    obj.set({
	    	top: canvas.height - (obj.height * obj.scaleY) - 68,
	    	selectable: true,
	    	hasBorders: true,
	    	hasRotatingPoint: false
	    });
	    canvas.setActiveObject(obj);
	    canvas.renderAll();
	});
	fabric.util.addListener(document.getElementById('icon_left'), 'click', function () {    
	    //canvas.setActiveObject(canvas.item(0));
	    var obj = canvas.getActiveObject();
	    obj.set({
	    	left: 300,
	    	selectable: true,
	    	hasBorders: true,
	    	hasRotatingPoint: false
	    });
	    canvas.setActiveObject(obj);
	    canvas.renderAll();
	});

	fabric.util.addListener(document.getElementById('icon_up'), 'click', function () {    
	    //canvas.setActiveObject(canvas.item(0));
	    var obj = canvas.getActiveObject();
	    obj.set({
	    	top: 150,
	    	selectable: true,
	    	hasBorders: true,
	    	hasRotatingPoint: false
	    });
	    canvas.setActiveObject(obj);
	    canvas.renderAll();
	});
	jQuery( "#add_text_input" ).click(function() {
		jQuery('#add_text_input').empty();
	});
	fabric.util.addListener(document.getElementById('icon_remove'), 'click', function () {    
	    //canvas.setActiveObject(canvas.item(0));
	    var obj = canvas.getActiveObject();
	    canvas.remove(obj);
	    jQuery(".image_options").hide();
	    canvas.renderAll();
	});
	fabric.util.addListener(document.getElementById('text_icon_remove'), 'click', function () {    
	    //canvas.setActiveObject(canvas.item(0));
	    var obj = canvas.getActiveObject();
	    canvas.remove(obj);
	    jQuery(".image_options").hide();
	    canvas.renderAll();
	});


	fabric.util.addListener(document.getElementById('situation_reverse'), 'click', function () {    
		var obj = canvas.getActiveObject();
		var filter = new fabric.Image.filters.Invert();
		obj.filters.push(filter);
		obj.applyFilters();
		canvas.renderAll();
	}); 
	//Text Align Left
	fabric.util.addListener(document.getElementById('text_align_left'), 'click', function () {    
		canvas.getActiveObject().textAlign = "left";
		canvas.renderAll();
	}); 
	//Text Align center
	fabric.util.addListener(document.getElementById('text_align_center'), 'click', function () {    
		canvas.getActiveObject().textAlign = "center";
		canvas.renderAll();
	}); 
	//Text Align Right
	fabric.util.addListener(document.getElementById('text_align_right'), 'click', function () {    
		canvas.getActiveObject().textAlign = "right";
		canvas.renderAll();
	});
  	//Image Flip Y
  	fabric.util.addListener(document.getElementById('icon_valign'), 'click', function () {    
  	 	var obj = canvas.getActiveObject();
		//obj.toggle('flipY');
		var curAngle = obj.angle;		
 		
 		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}
		var clipByName = function (ctx) {
			obj.setCoords();
			var scaleXTo1 = (1 / obj.scaleX);
			var scaleYTo1 = (1 / obj.scaleY);
			ctx.save();

			var ctxLeft = -( obj.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( obj.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(obj.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - obj.oCoords.tl.x + 7,
				boundingbox.top - obj.oCoords.tl.y + 7,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}
		console.log("boundingbox.left.."+boundingbox.left);
		console.log("boundingbox.top.."+boundingbox.top);
		console.log("boundingbox.width.."+boundingbox.width);
		console.log("boundingbox.height.."+boundingbox.height);
		//

		obj.set({
	    	angle: curAngle + parseInt(180),
	    	left: boundingbox.left + (boundingbox.width / parseInt(4)),
	    	top: boundingbox.top + (boundingbox.height / parseInt(4)),
	    	clipName: 'logo',
					clipTo: function(ctx) { 
					return _.bind(clipByName, logo)(ctx) 
				}
	    });
	    obj.setCoords();
		canvas.renderAll();
	});
	//Image Flip X
	fabric.util.addListener(document.getElementById('icon_halign'), 'click', function () {    
		var obj = canvas.getActiveObject();
		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}
		var clipByNameFlipX = function (ctx) {
			obj.setCoords();
			var scaleXTo1 = (1 / obj.scaleX);
			var scaleYTo1 = (1 / obj.scaleY);
			ctx.save();

			var ctxLeft = -( obj.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( obj.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			//ctx.rotate(degToRad(obj.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - obj.oCoords.tl.x + 1,
				boundingbox.top - obj.oCoords.tl.y + 1,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}
		obj.toggle('flipX');
		obj.set({
	    	left: boundingbox.left + (boundingbox.width / parseInt(4)),
	    	top: boundingbox.top + (boundingbox.height / parseInt(4)),
	    	clipName: 'logo',
					clipTo: function(ctx) { 
					return _.bind(clipByNameFlipX, logo)(ctx) 
				}
	    });
	    //obj.setCoords();		
		canvas.renderAll();
	}); 
  
  //Line Height Slider
  jQuery("#text_lineheight").slider({
  	range: "min",
  	value: 2,
  	min: 0.3,
  	max: 10,
  	step: 0.1,
  	animate: 200,
  	slide: function(event, ui) {
  		console.log(ui.value);
  		canvas.getActiveObject().lineHeight = ui.value;
  		canvas.renderAll();
  	}
  });  
  //Letter Spacing Slider
  jQuery("#text_letterspacing").slider({
  	range: "min",
  	value: 2,
  	min: 1,
  	max: 700,
  	step: 55,
  	animate: 200,
  	slide: function(event, ui) {
  		console.log(ui.value);
  		canvas.getActiveObject().charSpacing = ui.value;
  		canvas.getActiveObject().width = ui.value + parseInt(15);
  		canvas.renderAll();
  	}
  }); 
  // function textLineHeight(ui.value){
  //   console.log(this.value);
  //   canvas.getActiveObject().setLineHeight(this.value);
  //   canvas.renderAll();
  // }
  //Change font   
  var fonts = ["AmaticSC-Bold", "AnkaCLM-Bold", "MINIMALIST_1_", "Nehama", "SuezOne-Regular"];
    //$.each(fonts, function(index, value){
    	for(var i = 0 ; i < fonts.length; i++){
    		jQuery(".fonts_ul").append("<div class='fonts' style='font-family:"+ fonts[i] +"' data-val="+ fonts[i] +">"+ fonts[i] +"</div>");
    	}
    	jQuery(".text_fonts .fonts_ul div").click(function (event) {
	    //function selectFont(){  
	    	loadAndUse(jQuery(this).attr('data-val'));
	    }); 
    // fonts.forEach(function(font) {
    //   var option = document.createElement('option');
    //   option.innerHTML = font;
    //   option.value = font;
    //   select.appendChild(option);
    // });

    function loadAndUse(font) {
    	var myfont = new FontFaceObserver(font)
    	myfont.load()
    	.then(function() {
          // when font is loaded, use it.
          canvas.getActiveObject().set("fontFamily", font);
          canvas.requestRenderAll();
      }).catch(function(e) {
      	console.log(e)
          //alert('font loading failed ' + font);
      });
  } 
  jQuery("#text_align_center").addClass('active');
  jQuery(".text_align_icons div").click(function(){
  	jQuery("#text_align_left").removeClass('active');
  	jQuery("#text_align_center").removeClass('active');
  	jQuery("#text_align_right").removeClass('active');
  	jQuery(this).addClass('active');
  });

  // jQuery('contrast').onclick = function () {
  //   applyFilter(6, this.checked && new f.Contrast({
  //     contrast: parseFloat($('contrast-value').value)
  //   }));
  // };
  // jQuery('contrast-value').oninput = function() {
  //   applyFilterValue(6, 'contrast', parseFloat(this.value));
  // };
  
  var imageElement = document.createElement('img');
  imageElement.src = 'https://dev.kumailenterprises.com/wp-content/uploads/2021/02/Contact-Us_img.png';
  var fImage = new fabric.Image(imageElement);
  fImage.scaleX = 1;
  fImage.scaleY = 1;
  fImage.top = 150;
  fImage.left = 150;
  fImage.index = 0;

  fabric.util.addListener(document.getElementById('add_text_btn'), 'click', function () {      
    Addtext();  
  }); 

  //Curved text
  (function(fabric) {


fabric.TextCurved = fabric.util.createClass(fabric.Object, {
 	type: 'text-curved',
 	diameter: 250,
 	kerning: 0,
 	text: '',
 	flipped: false,
 	fill: '#000',
 	fontFamily: 'Nehama',
	fontSize: 24, // in px
	fontWeight: 'normal',
	fontStyle: '', // "normal", "italic" or "oblique".
	cacheProperties: fabric.Object.prototype.cacheProperties.concat('diameter', 'kerning', 'flipped', 'fill', 'fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'strokeStyle', 'strokeWidth'),
	strokeStyle: null,
	strokeWidth: 0,

	initialize: function(text, options) {
	  	options || (options = {});
	  	this.text = text;

	  	this.callSuper('initialize', options);
	  	this.set('lockUniScaling', true);

	    // Draw curved text here initially too, while we need to know the width and height.
	    var canvas = this.getCircularText();
	    this._trimCanvas(canvas);
	    this.set('width', canvas.width);
	    this.set('height', canvas.height);
	},

	_getFontDeclaration: function(){
		return [
	      // node-canvas needs "weight style", while browsers need "style weight"
	      (fabric.isLikelyNode ? this.fontWeight : this.fontStyle),
	      (fabric.isLikelyNode ? this.fontStyle : this.fontWeight),
	      this.fontSize + 'px',
	      (fabric.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
	      ].join(' ');
	},

	_trimCanvas: function(canvas){
	  	var ctx = canvas.getContext('2d'),
	  	w = canvas.width,
	  	h = canvas.height,
	  	pix = {x:[], y:[]}, n,
	  	imageData = ctx.getImageData(0,0,w,h),
	  	fn = function(a,b) { return a-b };

	  	for (var y = 0; y < h; y++) {
	  		for (var x = 0; x < w; x++) {
	  			if (imageData.data[((y * w + x) * 4)+3] > 0) {
	  				pix.x.push(x);
	  				pix.y.push(y);
	  			}
	  		}
	  	}
	  	pix.x.sort(fn);
	  	pix.y.sort(fn);
	  	n = pix.x.length-1;

	  	w = pix.x[n] - pix.x[0];
	  	h = pix.y[n] - pix.y[0];
	  	var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

	  	canvas.width = w;
	  	canvas.height = h;
	  	ctx.putImageData(cut, 0, 0);
	},

	// Source: http://jsfiddle.net/rbdszxjv/
	getCircularText: function(){
	  	var text = this.text,
	  	diameter = this.diameter,
	  	flipped = this.flipped,
	  	kerning = this.kerning,
	  	fill = this.fill,
	  	inwardFacing = true,
	  	startAngle = 0,
	  	canvas = fabric.util.createCanvasElement(),
	  	ctx = canvas.getContext('2d'),
	      cw, // character-width
	      x, // iterator
	      clockwise = -1; // draw clockwise for aligned right. Else Anticlockwise

	      if (flipped) {
	      	startAngle = 180;
	      	inwardFacing = false;
	      }

	    startAngle *= Math.PI / 180; // convert to radians

	    // Calc heigt of text in selected font:
	    var d = document.createElement('div');
	    d.style.fontFamily = this.fontFamily;
	    d.style.whiteSpace = 'nowrap';
	    d.style.fontSize = this.fontSize + 'px';
	    d.style.fontWeight = this.fontWeight;
	    d.style.fontStyle = this.fontStyle;
	    d.textContent = text;
	    document.body.appendChild(d);
	    var textHeight = d.offsetHeight;
	    document.body.removeChild(d);

	    canvas.width = canvas.height = diameter;
	    ctx.font = this._getFontDeclaration();

	    // Reverse letters for center inward.
	    if (inwardFacing) { 
	    	text = text.split('').reverse().join('') 
	    };

	    // Setup letters and positioning
	    ctx.translate(diameter / 2, diameter / 2); // Move to center
	    startAngle += (Math.PI * !inwardFacing); // Rotate 180 if outward
	    ctx.textBaseline = 'middle'; // Ensure we draw in exact center
	    ctx.textAlign = 'center'; // Ensure we draw in exact center

	    // rotate 50% of total angle for center alignment
	    for (x = 0; x < text.length; x++) {
	    	cw = ctx.measureText(text[x]).width;
	    	startAngle += ((cw + (x == text.length-1 ? 0 : kerning)) / (diameter / 2 - textHeight)) / 2 * -clockwise;
	    }

	    // Phew... now rotate into final start position
	    ctx.rotate(startAngle);

	    // Now for the fun bit: draw, rotate, and repeat
	    for (x = 0; x < text.length; x++) {
	      cw = ctx.measureText(text[x]).width; // half letter
	      // rotate half letter
	      ctx.rotate((cw/2) / (diameter / 2 - textHeight) * clockwise);
	      // draw the character at "top" or "bottom"
	      // depending on inward or outward facing

	      // Stroke
	      if (this.strokeStyle && this.strokeWidth) {
	      	ctx.strokeStyle = this.strokeStyle;
	      	ctx.lineWidth = this.strokeWidth;
	      	ctx.miterLimit = 2;
	      	ctx.strokeText(text[x], 0, (inwardFacing ? 1 : -1) * (0 - diameter / 2 + textHeight / 2));
	      }

	      // Actual text
	      ctx.fillStyle = fill;
	      ctx.fillText(text[x], 0, (inwardFacing ? 1 : -1) * (0 - diameter / 2 + textHeight / 2));

	      ctx.rotate((cw/2 + kerning) / (diameter / 2 - textHeight) * clockwise); // rotate half letter
	  	}
	  	return canvas;
	},

	_set: function(key, value) {
		switch(key) {
			case 'scaleX':
			this.fontSize *= value;
			this.diameter *= value;
			this.width *= value;
			this.scaleX = 1;
			if (this.width < 1) { this.width = 1; }
			break;

			case 'scaleY':
			this.height *= value;
			this.scaleY = 1;
			if (this.height < 1) { this.height = 1; }
			break;

			default:
			this.callSuper('_set', key, value);
			break;
		}
	},

	_render: function(ctx){
		var canvas = this.getCircularText();
		this._trimCanvas(canvas);

		this.set('width', canvas.width);
		this.set('height', canvas.height);

		ctx.drawImage(canvas, -this.width / 2, -this.height / 2, this.width, this.height);

		this.setCoords();
	},

	toObject: function(propertiesToInclude) {
		return this.callSuper('toObject', ['text', 'diameter', 'kerning', 'flipped', 'fill', 'fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'strokeStyle', 'strokeWidth', 'styles'].concat(propertiesToInclude));
	}
});

fabric.TextCurved.fromObject = function(object, callback, forceAsync) {
	return fabric.Object._fromObject('TextCurved', object, callback, forceAsync, 'text-curved');
};

})(typeof fabric !== 'undefined' ? fabric : require('fabric').fabric);


	function editObject(){
		var text = jQuery('#add_text_input').val();
		var fName = 'Arial';
		var fSize = jQuery('#fontSize').val();
		var diameter = jQuery('#diameter').val();
		var kerning = jQuery('#kerning').val();
		var flipped = jQuery('#flip').is(':checked');
		var obj = canvas.getActiveObject();

		//Get Bounding Box
		var selected_view = jQuery("input[name='select_side']:checked").val();
		if (selected_view == 'sidea'){
			var boundingbox = BoundingBoxCanvas1();
		}
		if (selected_view == 'sideb'){
			var boundingbox = BoundingBoxCanvas2();
		}
		var clipByName = function (ctx) {
			this.setCoords();
			var scaleXTo1 = (1 / this.scaleX);
			var scaleYTo1 = (1 / this.scaleY);
			ctx.save();

			var ctxLeft = -( this.width / 2 ) + boundingbox.strokeWidth;
			var ctxTop = -( this.height / 2 ) + boundingbox.strokeWidth;
			var ctxWidth = boundingbox.width - boundingbox.strokeWidth;
			var ctxHeight = boundingbox.height - boundingbox.strokeWidth;

			ctx.translate( ctxLeft, ctxTop );

			ctx.rotate(degToRad(this.angle * -1));
			ctx.scale(scaleXTo1, scaleYTo1);
			ctx.beginPath();
			ctx.rect(
				boundingbox.left - this.oCoords.tl.x -4,
				boundingbox.top - this.oCoords.tl.y - 3,
				boundingbox.width - 2,
				boundingbox.height - 2 
				);
			ctx.closePath();
			ctx.restore();
		}

		if (obj) {
			canvas.remove(obj);
	      // obj.set({
	      //   text: text,
	      //   diameter: jQuery('#diameter').val(),
	      //   fontSize: fSize,
	      //   fontFamily: fName,
	      //   kerning: kerning,
	      //   flipped: flipped
	      // });
	      // canvas.renderAll();
	    //}
	    //else {
	    	var obj_curved = new fabric.TextCurved(text, {
	    		diameter: jQuery('#diameter').val(),
	    		fontSize: fSize,
	    		fontFamily: fName,
	    		kerning: kerning,
	    		flipped: flipped,
	    		top: boundingbox.top + (boundingbox.height / 2),
	    		left: boundingbox.left + (boundingbox.width / 2)
	    	});
	    	obj_curved.clipTo = function(ctx) { 
			    return _.bind(clipByName, obj_curved)(ctx) 
			}
	    	canvas.add(obj_curved);
	    	canvas.setActiveObject(obj_curved);
	    }
	}

	// Create fabric canvas
	fabric.Object.prototype.objectCaching = false;

  // //Add a circular text
  // canvas.add(new fabric.TextCurved('It feels so nice to be able to draw', {
  //   diameter: 360,
  //   fontSize: 32,
  //   fontFamily: 'Arial',
  //   left: 50,
  //   top: 50,
  //   fill: 'red'
  // }));

  // // And another one
  // canvas.add(new fabric.TextCurved('any text around a circular path', {
  //   diameter: 360,
  //   fontSize: 32,
  //   fontFamily: 'Arial',
  //   left: 395,
  //   top: 405,
  //   fill: 'black',
  //   angle: -180
  //   //flipped: true
  // }));

	// Update controls
	function update(e) {
	    //var obj = e.target;
	    var obj = canvas.getActiveObject();
	    jQuery('#diameter').val(obj.diameter);
	    jQuery('#text').val(obj.text);
	    jQuery('#fontSize').val(obj.fontSize);
	    jQuery('#kerning').val(obj.kerning);
	    jQuery('#flip').prop('checked', obj.flipped);
	    canvas.setActiveObject(obj);
	}

	jQuery('#textbendingswitch').click(function() {
	    if(jQuery('#textbendingswitch').prop('checked')) {
		    var obj = canvas.getActiveObject();
		    canvas.remove(obj);
		    Addtext();
		    jQuery('#textbendingswitch').prop('checked', false)
		}
	});

	canvas.on({
		'object:selected': function(e) {
			update(e);
		},
		'selection:updated': function(e) {
			update(e);
		}
	})

	jQuery('#diameter,#fontSize,#kerning').on('input', editObject);
	jQuery('#text').on('keyup', editObject);
	jQuery('#flip,#newObject').click(function() { 
		editObject();
	});

	//Default Size and Side B Price
	var total_price_default = jQuery("#total_price_default").val();
	var side_b_price = document.getElementById('side_b_price').value; 
	total_price = parseFloat(40) + parseFloat(side_b_price) + parseFloat(total_price_default);
	jQuery("#total_price").val(total_price);
	jQuery(".product-price-container .price .amount:last").html(total_price);
	jQuery("h3 span#total_price").html(total_price);

	//Set Selected Size Price on selection
	jQuery('input[name="select_size"]').on('change', function() {
		var total_price_default = jQuery("#total_price_default").val();
		var selected_size = jQuery(".selsize_value input[type='radio']:checked").val();
		var side_b_price = document.getElementById('side_b_price').value;

		//console.log("selected_size.."+selected_size);
		if (selected_size == "30-40"){
			total_price_size = parseFloat(total_price_default) + parseFloat(40);
		}
		else if (selected_size == "32-22"){
			total_price_size = parseFloat(total_price_default) + parseFloat(50);
		}
		//Adding Side b Price
		total_price_modified = parseFloat(total_price_size) + parseFloat(side_b_price);

		jQuery("#total_price").val(total_price_modified);
		jQuery(".product-price-container .price .amount:last").html(total_price_modified);
		jQuery("h3 span#total_price").html(total_price_modified);
	});
  // jQuery(".add_to_cart_btn").click(function(){
  //   var total_price = jQuery("#total_price").html();
  //   jQuery.ajax({
  //     url : ajaxurl,
  //     type : 'POST',
  //     data: {
  //       action: 'read_me_later',
  //       total_price : total_price
  //           },
  //     success : function( response ) {
  //       //url = response;
  //       //jQuery("#rt_waiting").css({"display": "none"});
  //       console.log("total_price....Ajax.."+total_price);
  //       //window.open(url,"_self");
  //     },
  //     error: function(e) {
  //         console.log(e);
  //     }
  //   });
  // });
  //jQuery("#choptoolmain").hide();
  //Add Image Module
  jQuery(".upload-image-content").hide();
  jQuery( "span.container-popup-close" ).click(function(event) {
  	jQuery(this).parent().hide();
  });
  jQuery( "span.custom-action-close.fpd-btn1" ).click(function(event) {
   //jQuery(".row.single_product_tool .fpd-product-designer-wrapper").show();
   event.preventDefault();
   jQuery(".engraving_filters_parent").hide();
});
  jQuery( "a.upload-image-button" ).click(function() {
  	jQuery(".add-text-content").hide();
  	jQuery(".select-icon-content").hide();
   //jQuery(".row.single_product_tool .fpd-product-designer-wrapper").hide();
   jQuery(".upload-image-content").toggle();
});
  //Create Personal design Open
  jQuery( "a.button.primary.lowercase.create-personal-design" ).click(function() {
  	//jQuery("#choptoolmain").show();
  	    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    	if (isMobile) {
			jQuery("header#header,.text.sing-product-image-right,.product .shipping_section,#footer,.single_prod_main_col_resp").hide(); 
		}
  });
  //Create Personal design Close
  jQuery( ".create-personal-design.close" ).click(function() {
  	jQuery("#choptoolmain").hide();
  });
  //Add Text Module
  jQuery(".add-text-content").hide();
  jQuery( "a.add-text-button.tool-buttons" ).click(function() { 
  	jQuery(".upload-image-content").hide();
  	jQuery(".select-icon-content").hide();
  	jQuery(".add-text-content").toggle();
  });
  //Select Desings
  jQuery(".select-icon-content").hide();
  jQuery( "a.select-icon-button.tool-buttons" ).click(function() {
  	jQuery(".upload-image-content").hide();
  	jQuery(".add-text-content").hide();
  	jQuery(".select-icon-content").toggle();
  	jQuery(".image_options").hide();
  });

  jQuery( "#add_text_input" ).click(function() {
  	jQuery('#add_text_input').val('');
  });
  //Export Fabric Canvas to Image
  // fabric.Image.fromURL('https://via.placeholder.com/350x150', function(img){
  //   img.setWidth(200);
  //   img.setHeight(200);
  //   canvas.add(img);
  // }); 

	//jQuery(".add_to_cart_btn.tool-buttons-text").click(function(){
    //jQuery(".icon-box.featured-box.total_nis_btn.icon-box-right.text-right").click(function(){
    	//jQuery("#canvas").get(0).toBlob(function(blob){
      //saveAs(blob, "myIMG.png");
      //console.log(blob);
      
      // var reader = new FileReader();
      // reader.readAsDataURL(blob); 
      // reader.onloadend = function() {
      // 	var base64data = reader.result;                
      // 	console.log(base64data);
      // 	jQuery("#canvastoBlob").attr("src", base64data);
      // 	jQuery("#canvas_export").val(base64data);
      // }
  	//});    

    //}); 
})();
});