<?php
/**
 * Plugin Name:       Chop Tool
 * Plugin URI:        logicsbuffer.com/
 * Description:       Chop Board Designer Tool Maker [choptool]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       chop-tool
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
	add_shortcode( 'choptool', 'custom_fancy_product_form_single_chop' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script' );
	//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
	//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_read_me_later',  'read_me_later' );
	add_action( 'wp_ajax_nopriv_read_me_later', 'read_me_later' );

	//wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
	//wp_enqueue_script( 'my_voter_script' );

}

add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {

	add_menu_page(

	__( 'LB Icons', 'my-lbproductdesigner' ),

	__( 'LB Icons', 'my-lbproductdesigner' ),

	'manage_options',

	'lb-icons',

	'my_admin_page_contents',

	'dashicons-schedule',

	3

	);
}
function my_admin_page_contents() {

	?>
	<h1>
	<?php esc_html_e( 'Welcome to Logics Buffer Icons', 'my-plugin-lbproductdesigner' ); ?>
	</h1>
	<script type="text/javascript">
	//Uplaod Icon
    // on upload button click
	jQuery('body').on( 'click', '.misha-upl', function(e){
 
		e.preventDefault();
 
		var button = jQuery(this),
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() { // it also has "open" and "close" events
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			button.html('<img src="' + attachment.url + '">').next().val(attachment.id).next().show();
		}).open();
 
	});
 
	// on remove button click
	jQuery('body').on('click', '.misha-rmv', function(e){
 
		e.preventDefault();
 
		var button = $(this);
		button.next().val(''); // emptying the hidden field
		button.hide().prev().html('Upload image');
	});
	</script>

	<?php
	if( $image = wp_get_attachment_image_src( $image_id ) ) {
 
		echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
		      <a href="#" class="misha-rmv">Remove image</a>
		      <input type="hidden" name="misha-img" value="' . $image_id . '">';
	 
	} else {
	 
		echo '<a href="#" class="misha-upl">Upload image</a>
		      <a href="#" class="misha-rmv" style="display:none">Remove image</a>
		      <input type="hidden" name="misha-img" value="">';
	 
	}
}

 function custom_fancy_product_script() {
		        
	wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy.js',array(),time());	
	wp_enqueue_script( 'rt_custom_fancy_font_observer', plugins_url().'/custom_fancy_product/js/fontfaceobserver.js',array(),time());	
	
	//wp_enqueue_script( 'rt_fabric_script', plugins_url().'/custom_fancy_product/js/fabric.js');
	wp_enqueue_script( 'rt_fabric_script', plugins_url().'/custom_fancy_product/js/fabric2-4.min.js');
	
	wp_enqueue_script( 'rt_jquery_ui', plugins_url().'/custom_fancy_product/js/jquery-ui.js');
	wp_enqueue_script( 'rt_canvas_toblob', plugins_url().'/custom_fancy_product/js/canvas-toBlob.js');
	wp_enqueue_script( 'rt_canvas_customizecontrols', plugins_url().'/custom_fancy_product/js/customiseControls.min.js');
	//wp_enqueue_script( 'rt_fabric_script_angular', plugins_url().'/custom_fancy_product/js/angular.min.js',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/custom_fancy_product/css/custom_fancy.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style_responsive', plugins_url().'/custom_fancy_product/css/custom_fancy_responsive.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/custom_fancy_product/css/jquery-ui.min.css',array(),time());

	if( is_page( 'custom-banner')){
	wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/custom_fancy_product/css/bootstrap1.css');
	}
}

// add_filter( 'woocommerce_get_price_html', 'bbloomer_alter_price_display', 9999, 2 );
 
// function bbloomer_alter_price_display( $price_html, $product ) {
    
//     // ONLY IF PRICE NOT NULL
//     //if ( '' === $product->get_price() ) return $price_html;
//     global $product;
// 	$product_id = $product->get_id();

//     $total_price = get_post_meta('total_price_quote');
//     $orig_price = wc_get_price_to_display( $product_id );
//     $price_html = wc_price( $orig_price + $total_price );
        
//     return $price_html;
 
// }
// function return_custom_price($price, $product) {

//     global $product;
//     global $current_user;

//     $myPrice = get_post_meta('total_price_quote');
//     $price = $myPrice;
//     echo $price;
//     echo $price;
//     echo $price;
//     //$post_id = $product->get_id();
//     return $price;
// }

//Test

//add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

// add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );
// function add_custom_price( $cart_object ) {
// 	global $post;
// 	$product_id = $post->ID;    
//     $myPrice = get_post_meta('total_price_quote');

//     // Get the WC_Product Object instance
// 	$product = wc_get_product( $product_id );

// 	// Set the product active price (regular)
// 	$product->set_price( $myPrice );
// 	$product->set_regular_price( $myPrice ); // To be sure

// 	// Save product data (sync data and refresh caches)
// 	$product->save();

// }
//Test
// function woocommerce_custom_price_to_cart_item( $cart_object ) {  
//     if( !WC()->session->__isset( "reload_checkout" )) {
//         foreach ( $cart_object->cart_contents as $key => $value ) {
//             if( isset( $value["custom_price"] ) ) {
//                 //for woocommerce version lower than 3
//                 //$value['data']->price = $value["custom_price"];
//                 //for woocommerce version +3
//                 $value['data']->set_price($value["custom_price"]);
//             }
//         }  
//     }  
// }
// add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );
//function read_me_later(){
	// $total_price = $_REQUEST['total_price'];
	// update_post_meta('total_price_final', $total_price);
	
	// global $post;
	// global $woocommerce;

	// $product_id = $post->ID;   
	// echo $product_id;
	// echo $product_id;

	// echo $total_price;
	// echo $total_price;
	// //WC()->cart->add_to_cart($product_id);
	// $woocommerce->cart->add_to_cart( $product_id, 1 ); 
	//Simple, grouped and external products

	//add_custom_price();
	// $rand = rand(1, 9000);
	// // Insert the post into the database
	// $can_height = $_REQUEST['can_height'];
	// $product_title ="<p>Material: ".ucfirst($banner_material).'</br>'.$can_width.' '.ucfirst($size_unit).' &#10005; '.$can_height.' '.ucfirst($size_unit).'</br>Finishing: '.ucfirst($banner_finishing).'</br>Delivery: '.ucfirst($banner_delivery).'</br>Quantity: '.ucfirst($banner_qty)."</p>";
	// if($inch_height){
	// 	$can_height = floor($can_height).' foot-'.$inch_height.' inch';
	// 	update_post_meta($product_id, 'fpd_source_type', 'product'); 
	//die();
//}


function custom_fancy_product_form_single_chop() {
	$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
	global $post;
	global $woocommerce;
	//$product_id = $post->ID;

	if(isset($_POST['submit_prod'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		$quantity = 1;			 
		$product_id = $post->ID;
		$rt_total_price = $_REQUEST['total_price'];
		$selected_size = $_REQUEST['select_size'];
		$canvas_export = $_REQUEST['canvas_export'];
		//echo $rt_total_price;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity ); 
		update_post_meta($product_id, 'total_price', $rt_total_price);
		update_post_meta($product_id, 'selected_size', $selected_size);
		update_post_meta($product_id, 'canvas_export', $canvas_export);
		// $rt_total_price = $_POST['pricetotal_quote'];
		// echo $rt_total_price;
		// echo $product_id;
		   
		//Set price
		$product_id = $post->ID;    
	    //$myPrice = get_post_meta('total_price_quote');
	    $myPrice = $rt_total_price;

	    // Get the WC_Product Object instance
		$product = wc_get_product( $product_id );

		// Set the product active price (regular)
		//$product->set_price( $rt_total_price );
		//$product->set_regular_price( $rt_total_price ); // To be sure

		// Save product data (sync data and refresh caches)
		//$product->save();
		//Set price end

		//die();
		// Cart item data to send & save in order
		$cart_item_data = array('custom_price' => $rt_total_price);   
		// woocommerce function to add product into cart check its documentation also 
		// what we need here is only $product_id & $cart_item_data other can be default.
		WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		// // Calculate totals
		WC()->cart->calculate_totals();
		// // Save cart to session
		WC()->cart->set_session();
		// // Maybe set cart cookies
		WC()->cart->maybe_set_cart_cookies();	    

	}
	ob_start();
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	?>
<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
	<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						

<div id="choptoolmain" class="custom_calculator single_product">

	<!-- New Layout -->
	<div class="row single_product_tool_n" >
	   <div class="inside-container-create-d">
	      <a class="button primary lowercase create-personal-design close" style="">
	      <img id="close-design-tool" src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/Cross.png">  <span>Create Personal Design</span>
	      </a>
	   </div>
	   <div class="col small-12 large-9 tool-left-main" style="">
	   		<div class="image_options">
	   			<div class="contrast-control">
	   				<label>Image Contrast: <div id="remove-color-distance"></div></label>
	   			</div>
	   			<!--<input type="checkbox" id="invert"><button >Situation Reversal</button>-->

	   			<div id="situation_reverse_main">
				    <label><input type="checkbox" id="invert" class="visually-hidden"><span>Situation Reversal</span></label>
				</div>
	   			<div class="imageicons">
	   				<div id="icon_down" class="icons down">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="14" viewBox="0 0 10 14"><metadata></metadata><defs><filter id="filter" x="276" y="1492" width="10" height="14" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy_3" data-name="Shape 18 copy 3" class="cls-1" d="M280,1492v10.28l-4-3.89v2.75l5,4.86,5-4.86v-2.75l-4,3.89V1492h-2Z" transform="translate(-276 -1492)"/></svg>
	   				</div>
	   				<div id="icon_up" class="icons up">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="14" viewBox="0 0 10 14"><metadata></metadata><defs><filter id="filter" x="316" y="1492" width="10" height="14" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy_2" data-name="Shape 18 copy 2" class="cls-1" d="M322,1506v-10.28l4,3.89v-2.75l-5-4.86-5,4.86v2.75l4-3.89V1506h2Z" transform="translate(-316 -1492)"/></svg>
	   				</div>
	   				<div id="icon_left" class="icons left">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="10" viewBox="0 0 14 10"><metadata></metadata><defs><filter id="filter" x="354" y="1494" width="14" height="10" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy" data-name="Shape 18 copy" class="cls-1" d="M368,1498H357.719l3.89-4h-2.75L354,1499l4.863,5h2.75l-3.89-4H368v-2Z" transform="translate(-354 -1494)"/></svg>
	   				</div>
	   				<div id="icon_right" class="icons right">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="10" viewBox="0 0 14 10"><metadata></metadata><defs><filter id="filter" x="394" y="1494" width="14" height="10" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path class="cls-1" d="M394,1500h10.285l-3.89,4h2.75l4.863-5-4.863-5h-2.75l3.89,4H394v2Z" transform="translate(-394 -1494)"/></svg>
	   				</div>
	   				<div id="icon_remove" class="icons remove">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="18" viewBox="0 0 15 18"><metadata></metadata><image id="L0001" width="15" height="18" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAASCAQAAAAul0yEAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflBBMQCAzgOBtzAAABL0lEQVQoz63RsUtXYRTG8c9579VrIjjoECi8dwmpoamhNhtqjGgJhIYG/wMhtyaH+geCoL1Fam6p0QYXg0AIvBDkVg2KdfXet+VnDTX2Hc5wvstzzhMFtGHDXT8VYdZbj7seomgvql23YcuORu+SLdteGbsvkW956NSSRftOJKPaihMHGq8j73lmx+CHORUYHZnSWPKk9lEWGhTEZNKb8ynyFS999s4gzPpuXq8347JVa/KF/Dw/KPLVvFLkm0VezteKfCNv5/mkUYP77rWVzXbBqnWEMJMQEiq1pDalViMJknMGA84Uo+F8+Uf/k/+lKxWTUPXfOgmk318rwuSYHi/03Wm76as3dtGrpOTIodttdPvdQTvdvcdh9wF3fHMcRdt6atGxEMqklGmjR/Z+AaYLWTref/MaAAAAAElFTkSuQmCC"/></svg>
	   				</div>
	   				<div id="icon_valign" class="icons valign">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="17" viewBox="0 0 15 17"><metadata></metadata><defs><filter id="filter" x="333" y="1539" width="15" height="17" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1_copy_9" data-name="Forma 1 copy 9" class="cls-1" d="M334.154,1539l5.769,6.8,5.654-6.8H334.154Zm5.655,10.2-5.655,6.8h11.539Zm-3.232,5.67,3.232-3.86,3.461,3.86h-6.693Zm9.116-7.94v1.14h-1.154v-1.14h1.154Zm2.307,0v1.14h-1.153v-1.14H348Zm-4.615,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14h-1.154v-1.14h1.154Zm-2.307,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14H333v-1.14h1.153Z" transform="translate(-333 -1539)"/></svg>
	   				</div>
	   				<div id="icon_halign" class="icons halign">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="14.62" viewBox="0 0 17 14.62">
  <metadata></metadata><defs><filter id="filter" x="387" y="1539.38" width="17" height="14.62" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1_copy_8" data-name="Forma 1 copy 8" class="cls-1" d="M387,1552.87l6.8-5.62-6.8-5.51v11.13Zm10.2-5.51,6.8,5.51v-11.25Zm5.667,3.15-3.854-3.15,3.854-3.37v6.52Zm-7.934-8.89h1.134v1.13h-1.134v-1.13Zm0-2.25h1.134v1.13h-1.134v-1.13Zm0,4.5h1.134V1545h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134V1554h-1.134v-1.13Z" transform="translate(-387 -1539.38)"/></svg>
	   				</div>
	   			</div>
	   		</div>
	   		<div class="text_options">
	   			<div class="text_fonts">
	   				<div class="fonts_ul">  					
	   					
	   				</div>
	   			</div>
	   			<div class="text_align_icons">
	   				<div id="text_align_left" class="text-left">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="15" viewBox="0 0 17 15"><metadata></metadata><defs><style>.cls-1 {fill-rule: evenodd;}</style><filter id="filter" x="277" y="1293" width="17" height="15" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1_copy_6" data-name="Forma 1 copy 6" class="cls-1" d="M292.792,1305.56H278.208a1.22,1.22,0,0,0,0,2.44h14.584A1.22,1.22,0,0,0,292.792,1305.56Zm-14.584-5.94h14.584a1.215,1.215,0,0,0,0-2.43H278.208A1.215,1.215,0,0,0,278.208,1299.62Zm0,4.19h12a1.215,1.215,0,0,0,0-2.43h-12A1.215,1.215,0,0,0,278.208,1303.81Zm0-8.37h7.806a1.22,1.22,0,0,0,0-2.44h-7.806A1.22,1.22,0,0,0,278.208,1295.44Z" transform="translate(-277 -1293)" style="fill: #333333;filter: unset;"/></svg>
	   				</div>
	   				<div id="text_align_center" class="text-center">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="15" viewBox="0 0 17 15"><metadata></metadata><defs><style>.cls-1 {fill: #e75452;}</style><filter id="filter" x="332" y="1293" width="17" height="15" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#f77"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1" data-name="Forma 1" class="cls-1" d="M333.208,1295.44h14.584a1.22,1.22,0,0,0,0-2.44H333.208A1.22,1.22,0,0,0,333.208,1295.44Zm14.584,5.94H333.208a1.215,1.215,0,0,0,0,2.43h14.584A1.215,1.215,0,0,0,347.792,1301.38Zm-1.139-4.19h-12a1.215,1.215,0,0,0,0,2.43h12A1.215,1.215,0,0,0,346.653,1297.19Zm-1.968,8.37h-7.806a1.22,1.22,0,0,0,0,2.44h7.806A1.22,1.22,0,0,0,344.685,1305.56Z" transform="translate(-332 -1293)" style="fill: #333333;filter: unset;"/></svg>
	   				</div>
	   				<div id="text_align_right" class="text-right">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="15" viewBox="0 0 17 15"><metadata></metadata><defs><style>.cls-1 {fill-rule: evenodd;}</style><filter id="filter" x="387" y="1293" width="17" height="15" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1" data-name="Forma 1" class="cls-1" d="M388.208,1295.44h14.584a1.22,1.22,0,0,0,0-2.44H388.208A1.22,1.22,0,0,0,388.208,1295.44Zm14.584,5.94H388.208a1.215,1.215,0,0,0,0,2.43h14.584A1.215,1.215,0,0,0,402.792,1301.38Zm0-4.19h-12a1.215,1.215,0,0,0,0,2.43h12A1.215,1.215,0,0,0,402.792,1297.19Zm0,8.37h-7.806a1.22,1.22,0,0,0,0,2.44h7.806A1.22,1.22,0,0,0,402.792,1305.56Z" transform="translate(-387 -1293)" style="fill: #333333;filter: unset;" /></svg>
	   				</div>
	   			</div>
				<div class="letter_spacing_main">
	   				<label>Letter Spacing: <div id="text_letterspacing"></div></label>
	   			</div>
	   			<div class="line_height_main">
	   				<label>Line Height: <div id="text_lineheight"></div></label>
	   			</div>
	   			<div class="remove_bending"><span>Bending:</span><span><input type="checkbox" id="textbendingswitch"/></span></div>
	   			<div class="text_bending_main">
					<label>Text Bending<input type="range" id="diameter" min="10" max="300" value="15" step="10"></label>
				</div>
	   			<div class="texticons">
	   				<div id="text_icon_down" class="icons down">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="14" viewBox="0 0 10 14"><metadata></metadata><defs><filter id="filter" x="276" y="1492" width="10" height="14" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy_3" data-name="Shape 18 copy 3" class="cls-1" d="M280,1492v10.28l-4-3.89v2.75l5,4.86,5-4.86v-2.75l-4,3.89V1492h-2Z" transform="translate(-276 -1492)"/></svg>
	   				</div>
	   				<div id="text_icon_up" class="icons up">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="14" viewBox="0 0 10 14"><metadata></metadata><defs><filter id="filter" x="316" y="1492" width="10" height="14" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy_2" data-name="Shape 18 copy 2" class="cls-1" d="M322,1506v-10.28l4,3.89v-2.75l-5-4.86-5,4.86v2.75l4-3.89V1506h2Z" transform="translate(-316 -1492)"/></svg>
	   				</div>
	   				<div id="text_icon_left" class="icons left">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="10" viewBox="0 0 14 10"><metadata></metadata><defs><filter id="filter" x="354" y="1494" width="14" height="10" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Shape_18_copy" data-name="Shape 18 copy" class="cls-1" d="M368,1498H357.719l3.89-4h-2.75L354,1499l4.863,5h2.75l-3.89-4H368v-2Z" transform="translate(-354 -1494)"/></svg>
	   				</div>
	   				<div id="text_icon_right" class="icons right">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="10" viewBox="0 0 14 10"><metadata></metadata><defs><filter id="filter" x="394" y="1494" width="14" height="10" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path class="cls-1" d="M394,1500h10.285l-3.89,4h2.75l4.863-5-4.863-5h-2.75l3.89,4H394v2Z" transform="translate(-394 -1494)"/></svg>
	   				</div>
	   				<div id="text_icon_remove" class="icons remove">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="18" viewBox="0 0 15 18"><metadata></metadata><image id="L0001" width="15" height="18" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAASCAQAAAAul0yEAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflBBMQCAzgOBtzAAABL0lEQVQoz63RsUtXYRTG8c9579VrIjjoECi8dwmpoamhNhtqjGgJhIYG/wMhtyaH+geCoL1Fam6p0QYXg0AIvBDkVg2KdfXet+VnDTX2Hc5wvstzzhMFtGHDXT8VYdZbj7seomgvql23YcuORu+SLdteGbsvkW956NSSRftOJKPaihMHGq8j73lmx+CHORUYHZnSWPKk9lEWGhTEZNKb8ynyFS999s4gzPpuXq8347JVa/KF/Dw/KPLVvFLkm0VezteKfCNv5/mkUYP77rWVzXbBqnWEMJMQEiq1pDalViMJknMGA84Uo+F8+Uf/k/+lKxWTUPXfOgmk318rwuSYHi/03Wm76as3dtGrpOTIodttdPvdQTvdvcdh9wF3fHMcRdt6atGxEMqklGmjR/Z+AaYLWTref/MaAAAAAElFTkSuQmCC"/></svg>
	   				</div>
	   				<div id="text_center_h" class="icons center-h">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="14.62" viewBox="0 0 17 14.62"> <metadata></metadata> <defs><filter id="filter" x="387" y="1539.38" width="17" height="14.62" filterUnits="userSpaceOnUse"> <feFlood result="flood" flood-color="#1f1f1f"/> <feComposite result="composite" operator="in" in2="SourceGraphic"/> <feBlend result="blend" in2="SourceGraphic"/> </filter> </defs> <path id="Forma_1_copy_8" data-name="Forma 1 copy 8" class="cls-1" d="M387,1552.87l6.8-5.62-6.8-5.51v11.13Zm10.2-5.51,6.8,5.51v-11.25Zm5.667,3.15-3.854-3.15,3.854-3.37v6.52Zm-7.934-8.89h1.134v1.13h-1.134v-1.13Zm0-2.25h1.134v1.13h-1.134v-1.13Zm0,4.5h1.134V1545h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134v1.13h-1.134v-1.13Zm0,2.25h1.134V1554h-1.134v-1.13Z" transform="translate(-387 -1539.38)"/> </svg>
	   				</div>
	   				<div id="text_center_v" class="icons center-v">
	   					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="17" viewBox="0 0 15 17"><metadata></metadata><defs><filter id="filter" x="333" y="1539" width="15" height="17" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="Forma_1_copy_9" data-name="Forma 1 copy 9" class="cls-1" d="M334.154,1539l5.769,6.8,5.654-6.8H334.154Zm5.655,10.2-5.655,6.8h11.539Zm-3.232,5.67,3.232-3.86,3.461,3.86h-6.693Zm9.116-7.94v1.14h-1.154v-1.14h1.154Zm2.307,0v1.14h-1.153v-1.14H348Zm-4.615,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14h-1.154v-1.14h1.154Zm-2.307,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14h-1.154v-1.14h1.154Zm-2.308,0v1.14H333v-1.14h1.153Z" transform="translate(-333 -1539)"/></svg>
	   				</div>
	   			</div>
	   			<!-- Curved Text -->
				<!--<tr>
					<td>Text</td>
					<td><input type="text" id="text" value="Lorem ipsum di samet"></td>
				</tr>-->
					
				<div style="display: none;">
					<div>Font size</div>
					<div><input type="range" id="fontSize" min="8" max="100" value="24" step="1"></div>
				</div>
				<div style="display: none;">
					<div>Kerning</div>
					<div><input type="range" id="kerning" min="-10" max="10" value="0" step="1"></div>
				</div>
				<div style="display: none;">
					<div>Flip</div>
					<div><input type="checkbox" id="flip" value="1"></div>
				</div>
					
	   			<!-- Curved Text -->
	   		</div>
	   		<div class="design-area size">
	            <a class="button primary lowercase designarea-size-inner" style="padding:0p 0px 0px 0px;">
	                <span class="left-info-icon"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="15" viewBox="0 0 12.003 24.999"><path id="info" d="M444.642,69.3l-.38,1.428q-1.708.62-2.725.945a7.774,7.774,0,0,1-2.364.325,5,5,0,0,1-3.215-.928,2.907,2.907,0,0,1-1.147-2.358,7.679,7.679,0,0,1,.086-1.135,12.873,12.873,0,0,1,.277-1.309l1.423-4.636q.19-.666.32-1.262a5.06,5.06,0,0,0,.13-1.086,1.594,1.594,0,0,0-.4-1.239,2.394,2.394,0,0,0-1.522-.35,4.292,4.292,0,0,0-1.131.159c-.387.105-.718.207-1,.3l.381-1.43q1.4-.523,2.676-.9a8.646,8.646,0,0,1,2.42-.374,4.869,4.869,0,0,1,3.167.914,2.925,2.925,0,0,1,1.113,2.372c0,.2-.024.556-.077,1.064a6.612,6.612,0,0,1-.285,1.4l-1.417,4.617a11.432,11.432,0,0,0-.311,1.272,6.044,6.044,0,0,0-.139,1.079,1.484,1.484,0,0,0,.446,1.254,2.691,2.691,0,0,0,1.55.333,4.838,4.838,0,0,0,1.172-.167A7.021,7.021,0,0,0,444.642,69.3ZM445,49.919a2.611,2.611,0,0,1-.991,2.056,3.542,3.542,0,0,1-2.387.851,3.583,3.583,0,0,1-2.4-.851,2.619,2.619,0,0,1,0-4.119,3.77,3.77,0,0,1,4.786,0A2.635,2.635,0,0,1,445,49.919Z" transform="translate(-432.998 -47)" fill="#181818" style="fill: white;"></path></svg></span>  <span class="right-info-text">The size of the design area is <span class="size-sel-value">30 x 40</span></span>
	            </a>
	        </div>
	   		<canvas id="canvas1" width="810" height="500"></canvas>
	   		<canvas id="canvas2" width="810" height="500"></canvas>
		</div>
		<div class="tool-right small-12 large-3">
	       <div class="select-side-fancy" style="display: none;">
	         <div class="label">Select Side</div>
	         <div class="tool-right-views">
	            <div class="fpd-views-selection fpd-clearfix">
	               <div class="fpd-shadow-1 fpd-item fpd-tooltip" title="Front">
	                  <picture style="background-image: url(https://dev.kumailenterprises.com/wp-content/uploads/2021/02/cooking-board.png);"></picture>
	               </div>
	            </div>
	         </div>
	      </div>
	      <div class="select-side-wrap">
	         <div class="select-side-inner">
	            <div class="label select-side">Select Side</div>
	            <label for="sideb"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/cooking-board.png"> Side B <input type="radio" id="sideb" name="select_side" value="sideb" ></label>
	            <label for="sidea"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/cooking-board.png"> Side A <input type="radio" id="sidea" name="select_side" value="sidea" checked=""></label>                        
	         </div>
	      </div>
	      <div class="select-size-wrap">
	         <div class="label">Select Size of the product(cm)</div>
	            <div class="selsize-fields-container">
	                <div style="" class="select-size-wrapper">
	                    <div class="selsize_value">
	                        <div class="size_options_main"><label class="size-label">30x40 <input type="radio" class="select-size" name="select_size" value="30-40" checked="checked"></label></div>
	                        <div class="size_options_main"><label class="size-label">32x22 <input type="radio" class="select-size" name="select_size" value="32-22"></label></div>
	                    </div>
	                </div>
	            </div>
	      </div>
	      	
	        <div class="main-actions-parent">
		      	<!-- //Upload Image Button -->
				<?php
					global $product;
					$product_id = $product->get_id();
					$lb_image = get_field("image");
					if ($lb_image == "Show") {
				?>
					<a class="upload-image-button tool-buttons main-actions">
					 <span class="tool-buttons-icon">
					    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23" height="21" viewBox="0 0 23 21">
					       <defs>
					          <filter id="filter" x="1465" y="1333" width="23" height="21" filterUnits="userSpaceOnUse">
					             <feFlood result="flood" flood-color="#fff"></feFlood>
					             <feComposite result="composite" operator="in" in2="SourceGraphic"></feComposite>
					             <feBlend result="blend" in2="SourceGraphic"></feBlend>
					          </filter>
					       </defs>
					       <path id="Forma_1" data-name="Forma 1" class="cls-1" d="M1476.5,1339a5.68,5.68,0,1,0,5.62,5.68A5.657,5.657,0,0,0,1476.5,1339Zm0,9.54a3.86,3.86,0,1,1,3.82-3.86A3.841,3.841,0,0,1,1476.5,1348.54Zm8.8-12.27H1482a0.282,0.282,0,0,1-.25-0.15l-0.91-1.94c0-.01-0.01-0.01-0.01-0.02a2.057,2.057,0,0,0-1.86-1.16h-4.88a2.067,2.067,0,0,0-1.86,1.16v0.02l-0.92,1.94a0.282,0.282,0,0,1-.25.15h-3.36a2.721,2.721,0,0,0-2.7,2.73v12.27a2.714,2.714,0,0,0,2.7,2.73h17.6a2.714,2.714,0,0,0,2.7-2.73V1339A2.721,2.721,0,0,0,1485.3,1336.27Zm0.9,15a0.9,0.9,0,0,1-.9.91h-17.6a0.9,0.9,0,0,1-.9-0.91V1339a0.9,0.9,0,0,1,.9-0.91h3.36a2.063,2.063,0,0,0,1.86-1.16c0-.01.01-0.01,0.01-0.02l0.91-1.94a0.275,0.275,0,0,1,.25-0.15h4.88a0.265,0.265,0,0,1,.25.15l0.91,1.94a0.02,0.02,0,0,1,.01.02,2.085,2.085,0,0,0,1.86,1.16h3.3a0.9,0.9,0,0,1,.9.91v12.27Zm-3.59-11.36h1.8v1.82h-1.8v-1.82Z" transform="translate(-1465 -1333)" style="fill: white;"></path>
					    </svg>
					 </span>
					 <span class="tool-buttons-text">Upload a Photo</span>
					</a>
				<?php	
					}
				?>
				<!--//Add Text -->
				<?php
					global $product;
					$product_id = $product->get_id();
					$lb_text = get_field("text");
					if ($lb_text == "Show") {
				?>
					<a class="add-text-button tool-buttons main-actions">
						<span class="tool-buttons-icon">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="31" height="18" viewBox="0 0 31 18">
						   <image id="L1_copy" data-name="L1 copy" width="31" height="18" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAASCAQAAAAJOc1sAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflAhcLKSfQStm2AAACWUlEQVQ4y5WSa2gNYBzGfzsdOUlLIyuXtJgkVku0tJSSXJpbpORSiokoWZSiNMklKYRlND7gi/mAEvKBFKJt1iY5jm2nXWzOOWu26czm58POdCaKni//93361fO8/xdJ03q77LbLpBXD7v+q9EPIt35wo6u8bZsz/hdf7le3iDjKRk/+L37NiKNTc6UNv+Z/wscZ9px43udioT2pJEMa43nf+dGPhv3gQ+cPx7eaMMeQnX53sljjqzR4pmFjPvKgW9zjLZvtdGM6Xm21WGLCiNfFDXaYnfIyrbbebLHEMnem6rWZOwTn2+MG8YUPPGS7WeIny1NusUkPONVqW3xps08NON24u4bwi0bFXFstMs+E+8QKO1LuSqO2GbPZPLMssd8lBmzx1KAdTC2qzEanGPKVVQbNt8uDv8ple9RyH9hku/VOFVs9GwRgO5lcBQqYwF36mEYf2VRRy2qOAbCQM4wlRgNPCPOEKCFgIADADhoIA6VcoYZ33OYwCaCciRQAs7jBaHZTRJRJbOMxBfQDA4iFdrrrj98i5CevipVGDDjXmHVecJtJ1xm01ZNBYD1xbgL3mUcvGQAEGGANb3jOMnKZQRU/2E+Q2fxgE0nqBsMHyWQxT4lTxALOEWEkIH0c5wSLOM0KltLLCOAhSyijm83coZ4QGYRwkzHniM98Pyz4Pj+L+MjHHrfNcWKxtb7xiJfMF+usxNc2meNae9KWNNi7xTLz3GvcQmv9Yqm55nnEWr8bttRG4xi113a7bTL428NdNmmHX/xmseO9Z6cJ47Zb4VojRo1a8xNPdndRQLs9TQAAAABJRU5ErkJggg=="></image>
						</svg>
						</span>
						<span class="tool-buttons-text">Add a Text</span>
					</a>
				<?php	
					}
				?>
				<!--//Select Icon -->
				<?php
					global $product;
					$product_id = $product->get_id();
					$lb_icons = get_field("icons");
					$engraving_color = get_field("engraving_color");
					$engravingfeature = get_field("engravingfeature");
					if ($engravingfeature == "Enable") {
						?><input type="hidden" id="engraving_feature" value="<?php echo $engravingfeature; ?>" name=""><?php
					}else{
						?><input type="hidden" id="engraving_feature" value="<?php echo $engravingfeature; ?>" name=""><?php 
					}
					if ($lb_icons == "Show") {
					?>
					<a class="select-icon-button tool-buttons main-actions"><span class="tool-buttons-icon"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/select_icon.png"></span><span class="tool-buttons-text">Select an Icon</span></a>
					<input type="hidden" id="engraving_color" value="<?php echo $engraving_color; ?>" name="">
					<input type="hidden" id="side_a_image" value="<?php echo get_field('side_a_image'); ?>">
					<input type="hidden" id="side_b_image" value="<?php echo get_field('side_b_image'); ?>">
					<input type="hidden" id="side_a_bg_top" value="<?php echo get_field('side_a_bg_top'); ?>">
					<input type="hidden" id="side_a_bg_left" value="<?php echo get_field('side_a_bg_left'); ?>">
					<input type="hidden" id="side_a_bg_scale" value="<?php echo get_field('side_a_bg_scale'); ?>">

					<input type="hidden" id="side_b_bg_top" value="<?php echo get_field('side_b_bg_top'); ?>">
					<input type="hidden" id="side_b_bg_left" value="<?php echo get_field('side_b_bg_left'); ?>">
					<input type="hidden" id="side_b_bg_scale" value="<?php echo get_field('side_b_bg_scale'); ?>">

					<!-- Bounding Box Side A -->
					<input type="hidden" id="bounding_box_a_left" value="<?php echo get_field('bounding_box_a_left'); ?>">
					<input type="hidden" id="bounding_box_a_top" value="<?php echo get_field('bounding_box_a_top'); ?>">
					<input type="hidden" id="bounding_box_a_width" value="<?php echo get_field('bounding_box_a_width'); ?>">
					<input type="hidden" id="bounding_box_a_height" value="<?php echo get_field('bounding_box_a_height'); ?>">
					<!-- Bounding Box Side B -->
					<input type="hidden" id="bounding_box_b_left" value="<?php echo get_field('bounding_box_b_left'); ?>">
					<input type="hidden" id="bounding_box_b_top" value="<?php echo get_field('bounding_box_b_top'); ?>">
					<input type="hidden" id="bounding_box_b_width" value="<?php echo get_field('bounding_box_b_width'); ?>">
					<input type="hidden" id="bounding_box_b_height" value="<?php echo get_field('bounding_box_b_height'); ?>">
					<!-- Canvas Width Height -->
					<input type="hidden" id="canvas_width" value="<?php echo get_field('canvas_width'); ?>">
					<input type="hidden" id="canvas_height" value="<?php echo get_field('canvas_height'); ?>">

					<input type="hidden" id="side_b_price" value="<?php echo get_field('side_b_price'); ?>">
					<?php	
					}
				?>
				
			</div>

			<!--//Upload Image Content --> 
			<div class="upload-image-content" style="display: none;">
				<span class="container-popup-close">X</span>
				<h3>Select a Picture</h3>
				<p>By uploading the image i confirm that the image* rights belongs to me.</p>
				<!--<label for="file-upload" class="custom-file-upload">
				Attach a File <span class="attach-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 330.01 312.422"><g id="attach_file" transform="translate(0 -8.794)"><path id="XMLID_222_" d="M30.755,290.512a105.12,105.12,0,0,0,148.492,0L308.079,161.68A75,75,0,0,0,202.013,55.615L83.786,173.84a45,45,0,1,0,63.641,63.638l77.85-77.85a15,15,0,1,0-21.213-21.213l-77.85,77.85A15,15,0,1,1,105,195.052L223.226,76.828a45,45,0,0,1,63.64,63.64L158.034,269.3A75,75,0,1,1,51.968,163.232L180.8,34.4a15,15,0,1,0-21.213-21.213L30.755,142.019a105,105,0,0,0,0,148.493Z" fill="#fff"></path></g></svg></span>
				</label>
				<input type="file" id="file-upload" class="select_image" multiple>-->			
				<?php echo do_shortcode('[ajax-file-upload unique_identifier="my_contact_form" allowed_extensions="png,jpg,jpeg" disallow_remove_button="1" upload_button_value="upload" select_file_button_value="Attach a File"]');?>
				<div id="uploaded_images" class="uploaded_images"></div>
			</div>
			
			<!--// Add text content -->
			<div class="add-text-content" style="display: none;">
				<span class="container-popup-close">X</span>
				<h4>Enter Text</h4>
				<div class="fpd-module added_text_layers">
				   <div class="fpd-text-layers-panel">
				      <div class="fpd-scroll-area">
				         <div id="canvas_added_texts">                   	
				         </div>
				            <!--<input class="text_input" data-text="text1" value="text1" id="cardalltexthex1" />
							<input class="text_input" data-text="text2" value="text2" id="cardalltexthex2" />-->
				      </div>
				   </div>
				</div>
				<div class="add-text-buttons" data-type="text" style="">
				    <div class="add_text_inner">
				       <textarea id="add_text_input" rows="2" cols="50">Enter some text</textarea>
				       <a id="add_text_btn">Add Text</a>
				    </div>
				</div>
			</div>

			<!-- // Select Icon content -->
			<div class="select-icon-content" style="display: none;">
			<span class="container-popup-close">X</span>
			<h3>Select a Category</h3>
			<div class="select-icons-container">
			<div class="icons-category-list">
			   <div class="icons-list">
					<div class="animals_cat cat_parent">
						<div class="cat_icons">
							<div data-catname="animals"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30"><metadata></metadata><image id="Layer_162_copy" data-name="Layer 162 copy" width="30" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAQAAACROWYpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflBBcPGDAdjQa/AAAE3UlEQVQ4y4WUa2xTZRzGn3Pe03tH27GyrXR32Fg3mWNj3BzhMiQQRAISMUGRIFEw0UQJ0bhhoh8kIV4T0Q8GQoKXBI33wQiMMdwcUSwiY1vXtezWrV1vp+16OT3t64etOxhFnvPpffJ7/u/5v/9zXuY2JLEQSOvOTw+N5T9+ec9HehsFhYg0QkyowNnEmErbC3uy0gR0lifPIT33KGB95IN3nctpTn9DItfSRSJpAIBn+U/Hz77S1WSvK+83DwMAAwYMyDq4MQk33HAjpDr/Uvu2mapDFWYX+Y2nOQiWnPmwfQtlAV8ek13dpeLF2c04DgH4MVONyx2uzLSQ4rp3mb9eMKaU9+y9tjHj2lfYqwpHhNkVBwBxyJGGHFNazzzpBByWobqacX9Zxy7Ji1OvTIFkJswghXEYoMA0BLCMBEY0/QV9hG90VkoeoVyaAwWLNCi4meaTMCIEdSQ7IIFpLsANkumSpFzytImccA4oeGjAgp1DYUC5J29QAhnIUz741ffMEgtv6gdlCMILDiqwDNQqVYO6XqUSoY829Oj5DKhImP0ycIzUiTpe1aqbiGESKQBpcMTY+fLvO+Xi4rbCk0Fn5aWHr3TsmEHzHKVWbYofRTrzfnXX6i7yNA6CmYLEdPBMy3j+RK6jPmG2WHWjCpf/IbcJUEf3vGc4XyHqE72NXiMAlNw50myyOqCTxWppwUIPTZPAsQkLAIhkskJPcrtz7LW3tKwptO3TklOJaDGUPrPfWyajqzufeXvpZZ5OLOw5eqn5xu5odsVfjK6HX5HpqXh4z4urf06BNxjVHl8k7oaItZiW/1luM9TcLR6roH2ln711fVdcCShjh5qJ7tnpwkxY0OW5fL9cEc3x8rBD9EFEADJkpXhPcGRRSI2w7vuWC/tmRifK/OAWjXrmTjPJDC5O6GMxgh/AggXDEISoC9mgYKDArXWd21Nchp7Ssxs7SCqzpIhqIwoGNzAF6JIbE68nXxWXJWV6rIER0AytnzBLM1cmuLWtnRevbsl8FvMC+ugmeLmB5V2HB9b7THJqtm34Sjgpm6KI5A7XUCmLslFOPrrvhLvAVp0GoA+t7NzqYeT2p64ecVYDQIzhlziam8qa3mSdQlbint9GltzcTg5g/t2COyGDYMgObz3d+AlJdR049c5YsYSJrL1GyF/RkeO93jRSkXEf/fGJ98l+UGiGTZcNN2u+qf1cxnc/efq4x4h/iGLEwmU1dAu6G+tFAjBY9sfeY1l9ZD+AaUxFPQPaIZPQu+bcifFi/EuUsdeZVZu+DEfD5uxQ7YXdLeZfBXAEFAwEqLASKnP30bsW/Kfi7NndS744/JquLUgabrLeJAjIDsQQh4gFKJK1vfDd8yKL+yioViTqr2psToc6qsI8sCCbEUcKWugwsPRciysf9xfjLSrtrR3QIAkKPViwCcQRQxhhxdBjtir8ryZz+xp9yiKUwQQZWLBJJJFEGj7z7Q0CwQPUvyawSIQGBhAwYGOIIYY4/EXDhQ+KApNlkUIOFGkADDhhdpIxeUz54LBiMuxySVdvHgCAhb53Vc+3OyWM0NK+JY6s6ITRWh00Agwo8saf/rig14/MrcaR2Z3njx18Y35/2ypeqxKMPouz3pqyajyalFdZV2Pf3lkbUVXatp2tag0mMRf+G5zIBIYeUEZgAAAAAElFTkSuQmCC"/></svg>
							</div>
							<div data-catname="animals"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="39" height="31" viewBox="0 0 39 31"><metadata></metadata><image id="Layer_160_copy" data-name="Layer 160 copy" width="39" height="31" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAfCAQAAADOgxx+AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQflBBcPGS1nkFsnAAAF5ElEQVRIx42VW2wU1xnHfzNzdmavXl/WrG2ywWsbbFwbmxhUGxzcgIlza0JSiBqFpoKU9iFqoogoUR9aqWqlKG2pqlZq8tBEQbkoBLeEUJogJSkxMVjG2BiIDa4Jxuu18dq7vu2y19npg9cX2pD2P0/zzTe/c853zvc/0j9YUJocrJzEg5/vcQoJL+2UMYmHGMWMMYeOhJUcHGgM84lplXaf8mHCHlONEvLREXyNFMwopDNvEgoJEfTOlEUthoKhx4w5PWxJBldONHbdcUjkhjb35B4zTcgY/42Tsco3Cy7mzEwVjgoMTERtvmp9Y+A+9QEbJtJEGWWMKY5zKjPkCb77hvcXqj91K85AI+45tePzB/5QVOx/sNX8vi3sq4w/JTV8Z4sXbVlmiIvMIdPNDBDhvb1aquVnekhaqp1OEYG1v/ntP+/FBKBGdv6tLmCtaWpey9frAgdpZRgAd/Sl79d8qDy5bJnpold//9HDKBm86qu5a9OPSjzcTm7Wk4ePcSCZWt2zpVeWWHisdGw72bI8PUU+Tr5JK9jNc5QCKSlqTasitvjJZOlbOyXfusM66YXJ3kYOHifEK4RTeZMiIqaXalc0eY+hLk+14kbmf8nOk3QxNFfVFUiJpa0VkrAhLU+sY92tgduokD10xFYEViKKMiEJ+/U7W02lSetCkpcdlP0fMIAaBhVdMaE8hxkzZjSy0qbwleLxMiRQWc0ensDxDQidCcaYIo0VM4H4xJmCa8pWQoSYZBQV81jFkLDplXa5kafZjWvx1wjDDDOLxkJxY5zjIH+lna+IkIfZdr1WfCYWSq0gUYD59MbRgYe6HT+gYhEV5kva+RwfK3iYXeQDcIVf0UYVKseR2UczKrNOsVA5yTpYJhr70usLV6vjFC3CfLzLOwxTjper/JpZnsUKDNDJ3TxDlCMc5gDnqVVXFgpjHmcObJj7qWunkwGmYXFJo7zG64xTz88po5MDHGY7dRgIbKgEkXCTzSSfErQ2lMgCgZnp6sj+HY/spZkb/B09A0vxMQcZx0Y5Gp3E8TLJdUDCy3rmyKeUILNAAr+YdggnILi8SmmqMil42cA0KkZmbsfwAzphnLjwESFMAoBKfsyf+CNwIROR0BWhYmKk6r19WfZtVNHLIM0kiWEBBhgAIM5ZDuKlj3O4mbcElW3c5GW6l9pAN8eEH83Uv7Nne6V0hA7CVFHHSa5Qj8EEEQAMRngXKzNo/JBabtJFJ25qeBwfExlcwc27eoULc8pzunSgofxBwE4JKtlcpBob2rzxAWlChFBoZgc22niFszh5imYuc4wIOlq8+u3as8JBtiF/8cSZ6vKN/AszKhZqaOMsTZRRwOCyPnBxP+XAVS4xwTR9NPIolZwniH7j7o8tN4REr3Iuu0qovMWruHieFjwU00MOFbRwielFXClrMZCopoEghXybACovEKR1jGfqOlIou539uzqebdt0JvsIg/iJ0owDF1E+YYRskkQBCTNruAcZDRdOLMSoZj2zFGDhs7EbLzZ+4DRiiNOPHXh5xi1joGMQY5Yk4GAll/gdbipoIsggd7CPOnr4lCGiBNmDi35yiXL4qrJ/+1EnKUCcawm6l1uhhywgQR89RPiKAG6ymCKXXErJoYMRHNSziml8dHJttHF/89EUcTRAaDPLTaeEnViACH6CGQMII2EQ4S9Mkk0cM1HaaWWQfnzJbYeaTtgIoSJQEJv/vMLfWzuydc5p76/oLjB9saarFnQCrKOTeObcQSya+sB0Mp2WrGNlJ7Ze+tb8adtw9P4352IhdGRkUkiH0LSZnFWlM3li6PxIRKnPT3sxTLo1jWiveGvXlYZ54pZDP/nlmst2QnxpldaN1V/zJDXP0J3Hbf15pJAAAwnpHSRUNhNG532iPA2kURDE6ZNGG7ofu+CVpcqrm962XvAY2QQZpJIppd+ckEvjciJIMamlRpu36Thx0hhAAjBIYxAnadjPlJ/f6xg32sJZ0aixcA0lSejJSJIEpv+4mv4NQMMseNI6e4sAAAAASUVORK5CYII="/></svg>
							</div>
							<div data-catname="animals"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="41" viewBox="0 0 24 41"><metadata></metadata><image id="Layer_168_copy" data-name="Layer 168 copy" width="24" height="41" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAApCAYAAAAvUenwAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH5QQXDxodamQ4SAAAB5FJREFUSMeVlltsFOcVx//fN/cZey++7Rps2AVhB7ANawwV2M6FQsM9SslLaVWpL01phaq2qOpjqzz0pS0JVdS3SqUJKkrVkJJCiULAlKTG5uLEjvH9ur6t13vzemdmd2a+PoAbkwTvcj7Ny6cz53fOfOf7nyHt7e1IL6Zd598+/7Pb7e3f2LVrV+fRl469qShKxLIsEEKwbIwxaJqGT/77CYq1IjS3tuBf77+PzZs3Y2xsDIcOH0Y0GkV/Xz+q11UjlUyBTyQSmJme2Xnpvfd+kF5Mr5+dnmkKBoPhZ59/7s+2bduMsf8DOI5DOp2GntHhKipGIUaHB4eEDz/44NummV1fv60Bh44eKW9vbz8BgtJNNZsQ3BBEcEMQgWAAjU2NGBkextDAIARBKAwQCoW8t9s7GmqfqcXPT/8Cp395GrZtb5uPRquz2SxM04RhGOAFHkODQxgcHISiKgUFBwAai8f88/PzoT17duPAwQOwLOthQI6zKaWglMLtdiMei+PsmTfQ29MLWZYLBvCapqVKSrzJNWvWaowx3G5vh8DznRXl5ZOqqoJSiu7ubty4dh0T4xNwe9xYeS55AaVlZZGXXzn+WlFx0eH5SORIR3sHttbX3YvF4olYRycEQUB4chKTExMoKi56quAAQAFkNm7c8FbDtoYf93T3vDk2NpbZ0dT07ta6rXZVVRX+ffkyJEmCIIpPHRwAeF3X4TgsTSlN9/U9IJSScGlpyRQhBHNzs9ANHTzPE8pRiT40xnGcw/N8VhCEvES++7NuaJqG/oFBre9BX/WOpqYbHM/NfHzrY5SWloCjXNn01PSLyUTyhfRi2stx1IrH5ejMzEzf5MTEh4IgjIuimHlSdXx9fR0kSUJ/f/+60ZHReMuzre+EGhudu513xdGR0ecHBgZOnfvLX3fatu1jjgMCAJTiEncJHq+nd/+39t/UVPW8q9jVqWmakUgkHvuUfCwWgyTJmJ6abnUYW19SUjILEIyOjOw78/szvx0fH2twmAOCLyQDNpDNMegzmS1vn3try/VrH7W8ePBAh6pp131+31VFUeaXIdyunbuQiMeFgYGBk8lksiEUCv3BZjb94+tnz3Z91rUbYKCgIE9YDhwkUsmKrnv3Qzfb2o6FJ8OVgWAg7PP7pjOZDPj04iJEUeSXlpa4qXBYicfjVV2fdtX29PQ0EwAUNE8bPvRw4CAaj/GXr175PgBS+0ztrziOm+a+e+IE3F6vo2cyu25cv35wZGT4m/fv3jsWmYt4CFZIaR4jIKAgABhiC7H6eDxRtD6wfpbf3hiCLMuOwPOXPB7v8fufdtVQkLyZrzQGBgICURQRCAQ+b25t+Udra2sbLwgD3KlTp0AogcftiQBM7+/ta8wYmeKVAPZoLWe6vMdRDqqqOsUul11VXRVubmn+06snf/RGRUXF+bKysiHLsrM8YwyMMZimaTbu2PE30zCUCxcunJ6LzK1lADhQeDyeTKW/MrEQi2nzkYhbEITcpppNcxU+341sNjtdUVEe27t/350iTbuVy+ZMALBt+2Gbriw1lUolDx4+9Pq6QGDs6pUrP00mUx6327VU11B/bsuWrW0dt2+3/v2dd35XVlo2/OvXfvOdrXV1Ayd/+CqcR0kuP49dtMcOihDEYnEcf+X4xb17X/hnODzlqaz0pxRVtS6+exHbtm+PzM3N/cTlcg3qujG+uLgIPaPD4/U+WSq+0g2EIJPJIJlMOqlUKqYoMijHIRgMoKqqOj4VDqdcLtccpdRMJBLY3dyM3s97HpvdqwK+bI7jQFVV1NTUQJZl+Py+S6qqzvj9PqLrOjty9DBKS73ILGWgaerTAwBAEARIsgxZVeB2e246llVsGAbvOE7OMAxsD4WQy1lYiEafDkAIgWma+OjaNTDGQCiBntFNt9tVmk6necdxcst+PM9DlmV4S70YHxv/esDKLiCEgFKK2dlZWJb1cM8h4Hk+l8nomq4bAgD9ywkt6UuwbRs8z0PXDfCUUhBCwHEcVFWFoihgjKFyzRr4/P7HKlIUBbf+c0u9e+fOBlEQJXzduTIABAhPhr+ogNkOVE1F444QGAMc24Zh2195X5REROfnGwb6B3aKonieFCBVfGxhoZBzBgBks1lEo9GXLctap2maUciM5jmOKxhAKYUoCqbf75suKy83HcfJD6C0cNXkKAeO4yHJclaWZacggGmahQN4DqZhCpTSnChJzHkkaKsC0ul0wQDbsWEYhiQIwpIsS45dCGBxsXBALmeJDEwuKirKaEXFlv3ofqwKWFoqHJDNmgIv8KLL5dIVRS6sAj1jFAwwuaxIQEVJlrOiKLKCANHofF4nQgjAAFESBdu2JIEXbIEXGEfydyBfW1ub18l2HFBCEIlElHA4rAY3BLOSJKKgCva0NK/qwMBg5SyoioK2tjals7NTlkTJlGUZhmHk/ePmdV1f1YEQglQyibsjo+RB74PvLUSjpaZp5HRdx5q1ayEIwqqQvAOHEAIQ4u3p6X7uZtvNfVOTYamz487GIy8dVTwej55P8PIOnFwup44MD9fYtuVXFKWrwuebtB1nghIaYIwNMcZyq8X4H0W5iXPMPgzUAAAAAElFTkSuQmCC"/></svg>
							</div>
						</div>					
						<div class="cat_footer">
							<div class="cat_name">Animals</div>
						</div>
					</div>
					<div class="popular_cat cat_parent">
						<div class="cat_icons">
							<div data-catname="popular"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="30" viewBox="0 0 32 30"><image id="Layer_156_copy" data-name="Layer 156 copy" width="32" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAeCAYAAABNChwpAAAIpklEQVRIiY1XW2wT2Rk+tmc8F19iO87FV+KgmCxOQoASFIhYkEiIKEIsW6krLMpDS3mpuquqN+iiXrWs9mlFK63E9qGFQEHdRjxQ5Khi1yEhTTDhEnKBhETECR47djb2jC8z9tiu/mkmComz7C8d2R6fc/7v//7rKK5cuYLGxsZQKpVCFEWhmpoa6bO8vByBFItFVEqUSiU5NTV1kCCIRH19fV+hUCi5jyAINDo6ikRRhDNoeHgY6XQ6VFFRgbZs2YKwtQcwDEMMw6BQKITq6+sRz/NIoVC8tgd+F4vFTd3d3b8ym82zdXV1jxFC3Nq7YF8mk0HZbBapVKrShmxgnaRYEARE07RkhbxIkoTLlIFA4Pj09HTbs2fPjjx69Gg/KEmn0ysLGAX2/H4/evLkiXS2lKxjQKbt/v37kiu8Xi+Kx+Ov/dfb27utr6/vVGdnJ8OyrNrn8/3Y7Xb3I4SWZJfhOL4CBr5vJCUZgEtA0eLiInr58qXEBlAJK5lMqvv6+j7QarWU1+s9feLEibPxeHzvvXv3zsBZYAKY02g0aHp6Go2MjGxo/YYAQMD6QCCA7ty5IwFIJpPS5QMDAx1jY2Pf37dv3xeCIPy7trb2bw0NDY+Ghobe5zjOlc/nJaDgArAc1kaB/I0AZBbAkkQiIQFIJpPa3t7en9rt9mBjY+MlsDSbzeba29s/VigUNPwH8cNxHFpaWpJi6U3yxh2QFQsLC5JVDx48eDcYDL7d3t7+mUajeQ4AAYTL5fqqtbX1X8+fPz85Nze3MxqNIrVa/UblbwQALACFZrMZUtPt9/t/6fF4Hrvd7uvgIsgIoLxQKIgtLS2fGY3GvM/nO5dMJo16vV5i8Jvolwxcay1cnM/nnRiGVZMkaRsdHX1reHh488TExD6lUllz4MABL0VRDBQeoBg+QYnJZHqyd+/eS11dXR8ODw9b0un0MIZhMxRFTeA4HqZpOqRUKhfW1ZSurq5tU1NTbyWTSYxhGKsoio0cx+1IpVIVAIaiKEGr1S4SBDFntVpvtrW1XaMoKiNXPnANWq4dLMtWDQwMnGFZ9kA6na5KJBKmXC6nyuVyWb1eP0/T9AOlUvnM6XTGDAYDv3nz5jEsmUzu8Pv9n3Acp6qoqEhbrdag2+0eJUlyJpvNPrfZbLNgcSQSWcRxPFb8v6xYIAOBZyqVKmKxWD622WyfV1ZWmuPxuGNhYcFFUdSWeDxeOzc3tzcUCn1vYmICd7lc8w6H4yxmNptvNzc3b7158+bPwb8HDx48jRCKpdNpnuM4Qe4J4Ou19JWKGbAWx3FGp9MxGIY9FUVRaTAYSKvVStTW1m7v7u6+nEwmdS0tLRdVKtVdjCCISFtb24VMJoMPDg6edrlcXo/H8wej0SiVYagB3zaiVwswk8vlII0LFEWlIcSGhoZ+FIlEyrxe7/sOh+My1C0MUJMk+fX+/ft/n81mCZ/P9xu1Wp1vamr6SK/XFw0GA4K0gn2rGZDdsPb5cqOSQFdXV0tlHMdxU39//1+Ghobe6ezs/IXdbr8simIWGhQmo6Vpeunw4cPni8UicevWrbMkSaZ37tx5sVAo5C0WC6qqqlpRAKl59erVlZbd2Ngo3QGp19HRIQEAl0ExUqvVusHBwY/u3r179NChQ+cdDsfn+XxekPvDSh2AA3q9Pnb8+PFfOxyOL65fv/67kZGRMziOYxDh8gIAspVyAFZWVkoAgS2ZEdiL47h2cHDwQk9Pz8nW1tY/1tTU/FkURWG1q14rRABCq9UuHDt27KzNZrt148aN346Ojnrz+TwuNyNY0OFWZwJQKStetbSBQOBDUL5jx45PampqPhUEQVhbmDAoPvAQFnwHMZlMzNGjR89du3atrqen508syz7kef6pfAjog8YEioCFV69eSSzAM7Tcsl+8eNHh8/k+2Lp16z+2bdt2AXwuNyaZSUlnOBxe8RkEDlgDkY8QCm/atGma5/nadDpNyZdDZGu12pV4gF4A8XDq1CkEmQP7ltNRsxyIExUVFVm4c3XgwjlYWH9//2vRK4tGo9GzLGswmUwvwC1wMVgLh1aPV3AOFM3OzkpznsPhkHoEy7IzJEnGGIapnpiYUIqiWHJoxIC6tQKX8jxP8jxPZTKZxXA4nALFZWVlpe6Q9i/PCsjj8UjtOxqNLqlUqq8FQTDA1CSKIl8SgNVqXfcQfJTJZHTBYJBUq9WzRqMxDexAlMdisRU/guVyJgAr4J6HDx+iSCQCcZCiaTqOYZjWZrOBMaUBwPCwAQCXKIrVVVVVPc3NzWmYcGBEAyFJUsdx3IGxsbHDxWKRxXH8JkEQ/xVFsQgjN9QDjUaTiEajTCwWq8vlchaEULyEfoTJAbVaIFqhpwuCoHU6ncGlpaUipJ5Op6uYnZ3tHB8f987MzHyHIIhyYEChULynVqv9dXV1VymK6s1kMjxFUfHy8vJX8/PzLRzHGSAzSr07YCaTad1DoDYUCtE4jqvUajUXiUTUoVDoh/Pz8z9gGGYLTdPcrl27Pt2zZ8/TVCpF9PX1vT09PX1iZmamw2q1BhoaGi6RJHnbaDSyPM9jdrsdb2pqQqW8gEHErhWaphUKhaKMYRiYAX+SSCR+Fo1G6y0Wy7Db7T7H87zfarVOu93uHLgwGAzebmpquhwIBA6Fw+F3Jycn/+5wOJ4Ui8UyjuPUOI7TULaBxXUA1r6xQESLomgJh8N7xsfHMUEQtjc3N39VV1d3MZfLfanX6xfC4XABXrUg8pfzPlleXj7kdDoftbS0XB8ZGfluLBZ77/Hjx9thmA2Hw/sYhvFDi18HAIJrLQBoSARBcEeOHPnn7t27/5pKpQKFQiHBsmwBClYpWX6epShq0ul0zgAQj8fTPj4+flIURcXi4iKRzWbXA4D3v7WiUChCJpPp/OTkZBrDsCjUf0ixbyPLgSbCUGK327vcbvd/qqurVWazObUOPELof9tNxZbXRiqEAAAAAElFTkSuQmCC"/></svg>
							</div>
							<div data-catname="popular"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="28" height="28" viewBox="0 0 28 28"><image id="Layer_159_copy" data-name="Layer 159 copy" width="28" height="28" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAHcElEQVRIiY1WDUwb2RF+3l1Y/wDGBuyNjaGEGJmNLAEhSBEClzb8FBxICLS4RU0v0V1K1WuuzV3VKtdU0V0bXdWTql6bNlJlVYki3YVcUigJSYiCQ+BUMA4ch+Egh5NGpBjzUxs7xrv2rqtZeZHLpUlHGr39efO+mXnfzHuS8+fPo5SUFCSVSpFCocgbGRl5dWhoqDIQCORgGBbXarXeysrK3t27d19aWFj4d3V1NeI4DoXDYbS2toays7PRw4cP942Njb06PT1dEolEUkiSZHbt2uUuKyv7K0VRDp1OF0cIoR07diDcarUCmMTn833j4sWL5z0eT7vJZPLQNP2JwWBYjMfjBqfTeXRxcXGfTqebLC4uXo7H44hlWcTzvGx8fPwn3d3d5ziO+0pxcfEYTdMTFEUFA4HAV10u1xEcx7Nyc3M/k8vlIblcjgiVSoVmZmZsV69e/ZNOp/vnwYMHW0mSvMcwTCAajSKj0ag2m83NAwMD7/b29l5TKpWd6enpn2RmZkpv3779K4fD8ePGxsZLubm5Z1mWnVEoFHEcxyU0TZsePXr0g5s3b765tLS0+8iRI98jCMKHNzc3l0JkO3fu/KKjo6NNoVCMra+vMxAFALIsuwmRNTQ03J+amrIODQ1902QyocnJyc7+/v7XbDbbe7W1tSeXl5eXQqEQSk1NFVIej8dXCwsLb2o0Gt/o6GhXOByWlJWVDeFqtfr9YDBYZLPZvqNUKueePXuGNjc3kUQiEQ2FRWia/pfZbB5/8OBB2+Dg4LefPHlSbrVaf19ZWfkziUTCeb1ewUHgAgjP84gkSZSVleUiCEIzOTl5XCqVLmGzs7N7KioqrmRnZ0+BAYZhKFkAMBaLCarRaMaamprORiIRVFhY+I/6+vr34D8sDqMo8K5QKASVyWT83r17P9RoNGu3bt2yErFYLFWr1U5wHMcDGI7jW8ZgKJPJUEFBgeA9gJaUlPQeP368SKVSuSQSiQ9sgOWgIjhkRK1WC99AcByfVavVn8/Pz+cTBEEA0CbDMMKCIDk5OUKkT58+FYy0Wi2CqEBkMpmPpum3o9FoDFIODiY7mpmZKTzDfHFrGIaJ8TwficfjKUQ0GsWj0WgaTALvRI9hBBrDN3AEDMWoGYZhYUwWmAPZgDQmpxfsCIJI4TgOMFgiNTWVCYVCZqlUivM8zyUvQlEU2tjYQC8TAFAqlSgvL+9LgBBIKBTKD4VCeRRFfY4ZDIYvpqen6yKRiA48hPyLCozT6/XC/okRbhf4DunLysoSgILBIAKmiwr/5ufnq71eb351dfUwVl5e/rHH49GvrKx8DfKflpb2XyrW1YskkeYtB5KFZdkct9vdmp6e/qnJZLpM0DTdT1HUZ319fW9kZGT83Wg0rienRFwE9kisy2SBlAEjoWMlC3BgeXkZud3u5qmpqSqbzdal1WofYTKZzLt///4/zM3Nmdxu9w+hWEEhMhjFNIsESt43giCELMDi8AxkgxHsYGRZtvDOnTtvFRQUOCwWyxVgOwE5LikpuVJeXl5z48aNt2iaHpJIJA4whjT5/X6Unp4uLAag8CyCgSNwYsA/aOYwHyJeX1+Hk0ExPj7+S6/Xqz58+PBRlUq1KpRRV1cXMIsDBk1MTNS73e4DWq32LsuyqysrK8jj8QjphD4JTIRoIAIABoHFIeVAkEAgIMwFu+np6ZODg4MnLBbLmdLS0o/AcVAMPAVRq9UPrVbrCb/fn+Zyuf5IkqROTK/YSURCgA0oRAoOwHex+GH+xsbGK/fv3z9tNpvtNTU152AeRA/ZxOAFFMKFyOrq6n40NzdX7HA47DiO79jeW8X9204eEABbXFz8bn9//+/y8/N76uvrf87zfDhxdgpKJBsAKEVRf7NYLMTdu3fPBYPBj4uKio4hhGZfWBcIwX7iTqfzxL17997Nzc3tqa2tfZ3n+TWILNm5L7kPRa7X66+0tra+4vV6C5xOZzeO43ueFylKlAxJkikzMzOn+/r6fmMymT5sa2v7Psdxq2LDSNbnrgIp0Ov111tbW7/FMIzc4XB8hBDaCymDfdryFsOgbFKHh4d/feHChV9UVVV90NLS0oXjeOB/dafnu51oxlqtdqizs7Pd7/dvXr9+vdvn8+1bXV1N3kPpyMjIby9fvvxmXV3d2UOHDv0UIcRsb+z/F6AIqtfrXceOHeuMRqMhu91+6fHjx3sStwByYGDgnZ6entcbGxvfaW9vP41hWPRFYC8FRIn0UhT1aUdHx1GO42Kjo6N2kiS/PjExcbK7u/uNmpqa91taWs7E43FOPE9fJMRLZyRA4Xpx4MCB165du/aXU6dODYTD4VhpaemfGxoa3gawl0W2FSGQAIpYZKFYLyKVRXYl6tRRVVV1ZmFhIapUKoetVusZDMPgJN8iyPPqM1nwpqYm4QyD1gUpAXA4B4GRohPwHViXuEIswRWkoqKix2AwuETnwCF4hgBgPtzMk9cQRWK327ciEdsUqMjE7R7L5XKJ0WhMg7trOBxmk6MCOwCEnurz+VBGRsZWCxQEIfQfiqv6NGk1icoAAAAASUVORK5CYII="/></svg>
							</div>
							<div data-catname="popular"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="30" viewBox="0 0 18 30"><image id="Layer_158_copy" data-name="Layer 158 copy" width="18" height="30" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAeCAYAAAAhDE4sAAAFO0lEQVRIiZVVW2wTRxS9uzu7s/Y6MbZxfmqXSGnzUFUiCnkAaguBqkSFlIcoJEA+EqRSENC3kBpVVT/6kPpV9a8SSHy0gSbQ0qqxXcDlKUFCKAkSJKVJ7BgUIozfr/Wup5pxbCJIUDvSaqWd2TPnnnPuDPeH2wNPGzzPQzgSgXQ69fzZ02f2RiNRMxLFLHAAmpZFWMTxhsYGF6IL5xuEEPYWRdFyorf303NnvTslLAHHcZAjBDRVhbr6hiP2srKbKBKJzA0CBCRRAqvNCn96vTsvX7zYxqP8prlcDrSsBlXV1VfrGuq+AIBJlEwl5wQhOQKSRYLxsfGXT/T2vp/NqjzGMpvXNA3sZfbRne27Di1fseKOqqqAMMbzahONRhf9dOzYl8FgsFxRFPZd13UwKUpqQ8uGz1+srfUaZJmWDshsNj8BQjWQZRkd6+7eNzg4uNJgMBRLonNNa187sqyh/mSO5IDnOLYpKgg6e9AdBq9da3X93vd2gR1dp+k6LKur61m+orErmUwlJ31+mJ66z8DRbBiKTkv1+Xw13T/8+GEikSg1Go0MhOpQ8dxz5zo6OrokLIUCgQAkEwkIhULgcDgBaZpeBFpgLgWBF0rcfa5DvomJxYWSqC4Op/NuW9uOb0wmZcTn94Pf58szERAIggDocY3dLteeC+fPt2JZZgspCGX1yqpXf0Yi+u3ChYsQjoagRClh8/ShowiEEIKR0dGVx48f308IEXlBoImkWQBJxpBIxKqHhob2iaIoY4xpGTohhOi6LhBCriGB5yD4MAgmpcRxsqfnq3Ao5DQYDXkQABCQAIlYDDx97iZBQKupqQwdgKTTGb66puams3zRbcSoERC9Z07v+ev69UaWq8eMpJi6rnOapnOFb5lMhquoqLi1ecuW3Waz+QqiVg/097/V19d3QNdzPEKEJXd2yYUIFAZ10OF0jm7ZunWf1Wq9omYygOKx2AsD/f0fqRm1xGa1AAGOEeLzTCGrZSGbzRZFpSB2u320o7Nzf2VVldfv94PBIAMyGI3plo1vHpYkjASe1zgqAc8Dx0EmFAzWeNyeg2NjYyxfWTULFotlYntr68dLly71xBOJR8w5jvsHY/lburDAQiMErFYLhB+GNsTj8YMUlbIyLzAHNm7e9MmSl5b8Qssnudyj3MDMuUP7iGaGamJfaAObzWYaHr65fmpqCgSOA6NRedC0dm3X4trabrr+8dZivUbRaRdTVgvtC4Em+saNG5sGBvp3UG1ESYKWjS2HGxuXH1XVDMnN0Z+IMrBarVBZWUlFhHQ6DePjE894XO53opGIgmUMzevf+G57a9tngcAkyWTST4AwIJvNBk6nk7GixwLtm8uXLm0fHhqulyQJXl+37vuOzt1dGOPU7Fg8AZRMJiERTzB76Xl87969Wo/H3anputDc3Hy0s3P3B6WlpbFYLDovCANKJVNQEFxVVfzrqVN7J/3+mqY1a3qozQajMaaqmWKO5gdKp1hq6SUQfBBc5XF7Ourr6l272tvfVUym+9RNhETm6MyODJT+w3N80b3iCUkIKXO7XQfLy8uv7mjfdUAUxbvhcJjeZxCNRRkQXSsgRPsMwpEw6JrOnGZHLZbzh9ftW7fXqZk0v3XbtvcURfk7Ho+z78lkggFQFtQIfgaIukubAGOJzfOKSYG7gUDN6MhI7arVa742GuSrtJ941ib5n2lE6PtppfFGo8EwPT3dUFlV6XU4HN68xU8Xdk6xQ8GQEwn8Hafz2Sv56P9vjBnXUskwlvGEIPDZua6m/zQA4F9uWnwxZ1EQRAAAAABJRU5ErkJggg=="/></svg>
							</div>
						</div>											
						<div class="cat_footer">
							<div class="cat_name">Popular</div>						
						</div>
					</div>
					<div class="celebrity_cat cat_parent">
						<div class="cat_icons">
							<div data-catname="celebrity">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="37" height="32" viewBox="0 0 37 32"><image id="Layer_166_copy" data-name="Layer 166 copy" width="37" height="32" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAgCAYAAACVU7GwAAANJElEQVRYhZVXfWxb13U/74N8fI/fFMVPUSJF2ZbsWJKTSIoly7Xi2LHbJKjtGQgWJNlH0WwFhmD5Y1jRAUOxYH9tKAps2IAOWIAGnZsVWdrEbeNEdmM7ViTr0xIpS7JJSaRISpRI8Zvvke+94VyJgePkj+0KFyTEe8/9nXN+93fOpca/+AIoioJKpUJt7+x4MzvbA1upLbuW00hNTc1Zf3tgdmZqeutQ56GKwWBQxWoV3G4P3Lx5EzY3U/Ds6dNgtdkAVBVqUg1i8RjsZrPwRHc3bKY2YWdnB7RaDZjNFkgmEsziYlgQeMFw5uxZT1WscrIsR2VZ3uJ5Xob9wVarVdDr9ZDL5Q78/J133l5eXr6s0WhBURSgVBV0gnC3p7d30m5vukPT1FWBF7I0TcP/dTAMAzRN87lczr+9vf2dZCJxdH095v31Bx90A0Cz3W6/ffHSpX/rOdb7fqFQqKJtdjEUAofTCVvp9LlioXghEGgHoAAkUYRKuQKlcrnv1s2bfWO3P//LI08cvvLdixffNxpNv9FoNaKqqt8IBP+PYPR6Pb2xEQ+srq7+2ee3bp/aTm8PshoGdJwOTCYTaLVaKJVLJ25+9pnx+NDg2NzMbFTQC8AyDMuGFkJtxWLxNM/zLEXTgNNkMILlgI1sTCWTEI/HIbQQejkcXjx/+syZQ8Fg8GdarXbzm0BxHAelYlEILSy88uv/+eC1xEb8hKDXg9/vh5bWFjAajFAul8msiiJYzKb2+Eb8PAC8wzBMmU2mki9NTU79+cryyklFkUGWZaAZBrQaDRiNRuKRYDDAoc5O9ADWolHztd///sf+QPsT/QNP/4Tn+XEEgVwjfNBoQKrV3L+8cuUv/jB64y2z2WToG+gHHS9AbncXUqkURMoRKBWKIEkSKHtRNa4sr7zV2dmpHB8avMLoef6HkxMTfyRJkhbDjrNerwNyLZvNwtbWFjGWy+VISt1eL7g9HipfKDyRy+WHjCbjw2Kx+MDr9QLLsrCZSnE3Rq//cOn+0luB9oDgbWkhdlYjUYjHYhCPxSGZTEE+l8PLBdVKBQr5PGzEN2yZ3czAk089Nc22tQUia9FVKFUqoCK5KQqJSTiBqUOQ6NFmKgXb6TQkEglwOBzgcrtBUaFrMRz+J5fT+a1kMlGjgGYUWXazGuYFr9erw1v44MEDKBQKJJJoF6deEL7kHqgAGo2GnGWxWAsulzvLDg4NzouSCHNzs5DLZkFR9qKFYQUECUCAIkgc+XyeTEyD1+sBr9d9JBQOH5mZngFOq4Xjx48DQ7MQDoVgd3cXZEUBen8/Ooy2kSKAzlMUUAxFItzq80HPsWO/a2tru8eKNYl40t/XDwzLQCQSISmrlsqAv6n7IPe8UoGhaXQOisUiJJNJ8Hi8JBrZTAYEnidAcGl2d5c4hNwkgwKgqT0pQc7qdDoiRS2+Fug40AHFQhGJb75y5QrFqrKiLC8twUOGAafLCU6nE9pa2zC4xEvkl1yXoVavQUMC9oxTYDQaQMdxhGsk5SwLBrMZvB4PsBoW5Hp9L+KAkaJJRHANpgvXK6oCxUIBpu9OQ2ozBR0dHbxeb2DZSqWsQ6OlYpGQb31tnRzEajXA63jQG/QgCHri8V4K4EueYYTrskyEVlUVchBGZ3Nzk0QSbyVF3Nu7POVSCcqVCpSKJahWK8RRSZSINKC9pqYm5q1LF2l2bXWtpSED+InIcTZCztAIhCamkROwzzHkoUFvyAx/6+S43+83VMplr9ViKZnN5ofzC/NH52fnghiRvT0q4WqD3HtO7EcdHdnnXDaTeTK9tRVgV1ZWDmCYG+PxEoJAZVkiRnAzpgC/ixUROg91hQIB/5ugqKWeY8cOazWavMPpDOsN+u/Nzcz+Y7Va5XE9hpekUlG+vNkNMI+OQqnQur6+dpD2tfpiiorMUvY8UPY8QTCET7JMrquvtRVzDjqeJ7+j99293asOhzMpSWKiVqt9KknShF6vL7YHg+NmszmB6xAAAvP5fFv+QGDNbDZnZEWR0XatViNn4MT0Ndubx3ytrfPs6eee++VqJNq7ML9wQVVVipBgj5iq0+mMtgeD6cOHD2fdXq9t8u54f2x9nRjDom23N+dbfD65PdhBuGJragKaoSGTTpecDke5VCqB0WSCcrEEPb297w0ODf7Xvbl7/kqlMhSLxYZ2trfNlUpFR1GU2uxwhEZGRv7Z620JsTabbfnlV175u9np6Wg8FutTKQCL2ZL3twfuMgxz3etpiXZ1HtpYj8cuh8OL72HIkcD0XsGltFqtcuLkSYhGImA2m0kaTEZjWafjqnjlPR4PrEajKB9CsKNjLJFI3hkcHPzw3vy8ZzUScabTaSPDMOrA8WeiNpvtvqIoKovpcbldi+e/8+0fTd+dbEY+ujyu/KmRkfz42BipZTs7O/wv3n33THprC3qfPAarq2uQTm/tCgZhxmAw1rEkdR7ugrmZWXIpGIaWtBxXRfCtba2k71peXj4fDoXP+AP+a4VioVAuFpdoml5qXBz8wzQiX2nMu1gVUaXFcrkcF0UxrmE1+anJSbLA5/PRH3/88V/PTM+83N3dAyPPPgsalgVFVrIMRS/KiizLikxqZVWsQqVaBpUCRafj63W5Dna7HZwOJ9QkyX3j+vXTra2tRDjtzXYiBY/eyMZgzRYLEbFSoQRWWxNEVyNEe9LpNALiPvrNhz/46MMP/8bldhtffOklQACo+FarVU4kEkqxVCTiiqORPkGvrwp6IVMulUGr5cDX1goPIxFYWrrft53e9vsD/lUUaSzIo5+Ooq2v3EIaCyEix08tqrMkQV2pg9VqcVz96Orf/vd77/19vV43P3/ueTg+NEi4k9nZAZPJXOV1QkXDagnpOU4HNpuNTHtTU8ZqsU4W84U66llbmx94nsd9g6Ojn/xArzeQ886cPQsDzwwQoW2kkXSe+yJGq6pqUlXVxul0hnuz9048WF5+IRwKDcuybOgbGIBLly8TJZ6bmQNRksBitYreFm8Z+636vgatLK8Q41pOq1RFMccwdD2T3maPjhwFl9OJoLi743f/eGpy6t/NFlOE53WALQ86pdNxkN3ZIf09UUqGZlSapoR8fvdUKpH82czk1E8iDx6eVxTVEAgE4PtvvIGphLGxOxAKh4juOJ3OKb1ev9kQXAS1ubVJZjKRQJ6UeUGQ1tbXwNpkhc6uLrIvlUq5Rj/55EdAAdcgNmYKo4Q2SB3FdImiqCqKstnTe+zKX7355r9cunyxZLVZob2jfed7b3x/Y2CgHybGx+HG6ChwnBZ7+lJrq2+cZdkc8kIURUimkuAP+MkMBNvB43FnOY2mura2DjvbO/Dk00+BxWpBjWPGx784N3Zn7AWNRsOgQ4/2+qRqbKXTpLnjOE42Go3lQiEfyWazyY6DByeGh0+8ffDgwft3JyZGb926aTja051vdjg7FhcX0063a67xWsGBxG2UDeSLoqq7Or1eiq2twcL8PHz3wgU4cuQI3Pn8c8hkMp7fXf3tj61mi+R0OX/LMIz8KNGZ1159da/ZoiitKIntDx9GDmzEN66/8OKL/8oyTAg3xGKx296Wlv/o7+//aXh+ocNoNC4+M/DMf1IUpTRqGb7vSHtbrYIoVvEN2PxgZeVCIpGwoeT09PTCgYMHYGpqikjB7u6uIxKNHqvV6wW3xxPX6TiUJJWUptdfe43kWq7X+cVQ2OVwOlYFg3BHVdSCy+XEay53HTmS8no82empqadDC2H9iRPD7/r9/k0UPE7LkbmyvExuEbYnhXwBo8WmUqmTsfVY+x5QEc6ee55cisXFRVKgJUmyx+OxE4qsBO3N9pzBYEiwLCuzX/bKAEWNRjOLSOu1vSIZDAYJifH7/aWlzrX19Z6+gb7bNpstjIeQjnS/pcEa96gA8oKwYTSa7ut0uufw/zeuXydrTo2cymcy2cqn165ZnU6ndvjkSbvRZOys1euKyWIBq8UC7ONvNjwAyY+ExiKrYLcgy1a8NYFA4AuzxTJXyhe/0luhklsslq8QltPp5GAwGLo7MZEv5POm5ubmrWg0ku7u6f7p8MnhFavFdOx++P6f1uvy0ZOnTiU2U6lbxUKh7mtp+TooBNTZ1UlSgjwh7QwA3dLSMiNJUg5T8XgfVKtJ5H346MAGz+/3X7OYzX8iVqsDr77++tu+Vt91h8OxWCmXlaf6nv7M5fbcXF9bf2X8zpjd2tT0nIZll2VZ3vwaKASB9QrJiQD3o5EVBEEpV8qPLycA6/W9RvDRgS9fq80WO9rdfTVz4w8DWKDnZudCXV2dcLCzE7Q8r1Yq4lS1Wg2pqupLxGKmZoeDIZLwtVP2u81Gp7l/sPIoXxoDHxC5Yh7qCj6ZHv8Vn2lKbWj4xC8yO5ng+7/61Uhzc3PZ5XLeA4pa5jhORKdlWa5qddoV1C/kLsblG0H9f0YD7OMpJbCwDaHph+e+fe4fopHosKwogsls1mtYVuE5Dh8KkNjYaOyXiQ0A+F9eISB5qkO2LQAAAABJRU5ErkJggg=="/></svg>
							</div>
							<div data-catname="celebrity">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="37" height="25" viewBox="0 0 37 25"><image id="Layer_173_copy" data-name="Layer 173 copy" width="37" height="25" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAZCAYAAAC2JufVAAAIeklEQVRIiY1WW2wT6RX+PRfb4wuJHcc4hjgmF8d2qJ1AIISNayIVEOaipTSIliIEqkSk9qGrfahEV+KhPLTStts+bKVGWbVSW6E2CFKxNKIBVC0JGwFhg8kFEttx7FywPY4zHnuceOxxdaY4DSGEHunX2DPnP9fvXCT37t1DEokEcRyHCYKAcBwXhoeHkU6nQ7t27UJ+vx+l02lUVlaG8vk8isViqKKiAi0tLSGCIJBGoxGfqVQKVVdXo5WVFRQIBFChUBDvBINBZDQaRT6v14symQxyuVzAQ6jVaoNcLq/NZrPTOI7PoNdE8DyPMAwDxU2xWMy1ZcuWp4VCwUeSZEYqlSZwHBeN3ogkr98DD4ZhJI7jPMhaT/CdJEkCx3GNIAgKlmWrZ2dn2/r7+/en0+m25ubmzpKSkhlwWjQKPAJPI5FI7dWrV3+l0+nmHQ7HSxzHmWAweJ3jOC+GYQWZTBbneT4uWWNgLpcDw2Q0Te9MJpMnS0tLuwRBCAEPHJlMJqEoqjqZTOLLy8sd0Wh0z+TkpKq/v7/c5/M5ZmZmUH19/URDQ8OYXq9HECDRqMrKSiSVSiHcQwaD4bHX6/0gHA6bBwcHUUlJyYGamprZ2tpaxLKsTyqVfkEQBK9SqSIrKytTmUyGz2Qy9UtLS59RFOUaHx9v0Gq1P9doNFgsFpNPTk62xePx02NjY/KpqammRCKB4vG4mPoiOZ3ORwaDwQ9phwCJRjEMI6ZPqVTOHz58uHdiYmIvy7Iky7IoHA7rX7x4oddqtUilUu3S6XT7bTYbPjc394rn+X+RJDmez+e/b7FYXCaTCfX19X0YDoercRzHQ6EQ5fV6jQzDyEEHnPVUWloab29v77XZbGwymVyFAwHAA5LL5fy+fft67Xb79549e9ayNkXRaFQ8gUBg+/PnzyGyFUqlspGiqDSGYSq32z2t1Wq5Gzdu1LEs61heXhYBD8/NyOl0DjU1NT0ApxUKxep7Ai4DZbNZpNVqA263+5+jo6N78/n8hugGJ+AwDAPfVS6X6x8EQXyWy+USzc3NP+zp6enM5/PqTa1BCDKTcLlcXxIEEZ+amlpNnVgYV65cAWPEYzAYCkqlMj48PNxM0/S2zYRSFJU6ffp018mTJ3/Bsuw3Uqk04vF4hnbu3BkKh8MN8Xhcu9n9+vr6kaNHj/6mtLR0ESIKGSkeQi6XrzJCTnfs2DHa3t7eHwwGdy0vLxMbyCtUVVW9Onv27KeHDh3qisfjKehv4KlEImGbmpr+ZLVan3V1df1kaGjou0tLSyUbOCQcOHDgUWVl5dTi4uIbURIjdenSJbEU4YCVALhCobAQCARaI5GIcZ2wfEtLy5ednZ0fOxyOvwmCkIW0z87Oir0IKhkUaDSaBZPJNFhWVjbNcdwWhmFM+Xx+tYHV1dWNnzp16hOKoiLF3rSWiNHR0f+FoFAQK7G6unqivb39ic/n281xnIgtrVab8Xg8X7S0tPzaaDQGAYtrwflGKAsFADptMBj+eOLEiWGfz/fju3fvnltcXKTgu9VqDdtstgkIADjzllHF6ltLoNBut/cajUaPz+czlZeXv7x48eIfnE7n7/1+/wqMDck7uvxagrQyDOP1eDwf1dbWjnd3d3fSNG1Vq9XLCoWCX5+2VaNgtm0kTK/XewG8crm8orOz86rdbv8L9Jr/x5giAS/AgqZpbvfu3b9Lp9N8d3f3bxOJhGJubo7gOC63/o44YZxO51vCoMMHAgFIXa3FYvl6z549N6FPgbEbYeBdBBFvaGhAMBFA5pEjR3pv3brVyTDM1mg0qslkMrH1TlAUhYiampq3vFcqlWhiYsINc+3cuXOfC4KQLikpEflIkhSVwfNd4S96DKUOG8X27dvFO4IgzB87duzawMDAUYZhyliWjYFM4AWjYfxYrVZEQATWCwcALyws6LZu3fqitbX1ARgEUQIB8Hy94sC6AzPxjftFJSqVCu3fv19UFolEVouopaXlxqNHj46xLPthY2PjL6F6wUGYFMVRJAId9hwQUqwEkiSl4XDYaLPZfCMjI2nYIjaKCryDdEIroGlaxA9UM8iBb/As9rCiwYIgRE0m09L09PQHsFfBriaTyd6QiwEjeAvCPR4P5B22A2MwGKywWCyjMN+KAtcf8By8BBxAE4Z3YAgccKT4H37Dgd8KhSJVV1fnffr0qTMQCIibAyyORZlipIoKoXFC+ABPs7Ozrng8ntHr9V/V1dXli3vOZgRbAjgG2+pmWCNJkvf7/f2Li4s/GhkZ6WhsbPwGsrSWRKPAYzBofHwcqdVq1e3bt8+TJLl9bm6OBk9A2UYb5VqCggGe91Xn62JZwDAsde3atROpVGrwzJkzDymKSrzeYBEB4QcwUhQlxzCs9vr16xcGBgZcZrPZz/N8HkAPgt63hhRTsBmBQpCTSqUaEomEjGEY8/3797tzudwDiUTyhOf5NMMww9A8vzM5OdnOsqyWpunWx48ffyuXy2Hz8/P6dDp98ODBgy8gHbAxbkZDQ0PvjRI4HwqFqp48eXI+mUxuhXfBYNAQDAY7EEIdDocDmuxPCZqm6/r7+38Wi8XeGEIcx5X19PR8bDAYIsePH//7+yIFUdgMS+i/i6RubGzso8HBwW8XCoVVPBAEkWtra/vKZrP91W639+GXL18OOhyOYalUKoRCoR08z0uLzMlksnRsbKwVx/GExWKZRAjxG1UhnIWFBbH830USiWRHX1/fJ3fu3OnM5XLiYC4vL492dHT82e12f+p2uz9Xq9X/NpvNKcnDhw/FHuPz+TRGo7F1ZGTEOjMzo3358uWJcDhsy2azWFVVVejChQs/EATh63cpNZvNm2IK8Hrz5s3zHMeVGgyGVyaT6dW2bdsmstnss+np6TRUL0Bk79696D/XjXyv3GWx5wAAAABJRU5ErkJggg=="/></svg>
							</div>
							<div data-catname="celebrity">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="34" viewBox="0 0 13 34"><image id="Layer_154_copy" data-name="Layer 154 copy" width="13" height="34" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAiCAYAAACEEVOfAAAFVUlEQVQ4jV1VbUxTZxQ+vbftbe13SymUNqUFjEbHptQPls6J2cICkcyqITMuwRgdiZHEaEyWJWqIkiXzh9n4NTd/uEwZZM6hP5rMSBjRaYmDUHABCtJSOkAu/aC3X/fe3uVcBhHf5Py4b9/nfDznOaeSoaEhePNIJBIQBAGGhobeu3HjxlWPx9N/+vTpb3ieX38jzWazG0AymQw4jlM+ePDgi5GRkUae50sOHTr0Yz6fX14HpdPpDSClUgnBYPDdJ0+eNLlcLrBYLJtfvHjxaVlZ2a1CobAKGh0dfTsSMTY29pHdbrdeu3YN1Gq1ZnBw0Gm1WjGDVdDz58/frolKJBIur9cL9fX1EIlEwO/3q/P5PMn/X5g0kUhsABUKBUKhUMhramrE77m5OZiamvrQ6XRWsCw7IYKwhjcPy7IFi8XClpeXi98jIyPw6tWrKaVSOb9OhF6vfxvEm0wmBu8xtbt374JCoYjFYrFULpdbBRmNxg09wrzVavVKKBSC3t5eePr0KX/q1KlpmqYLa72SIq14sKFIqUQi4bPZLN3Z2QkPHz4Es9kcqq2t/Y0gCPGNCNqxYweQJAk0TQN6xxSmp6dVPT09kM1mC16v91ePxzO+QRFv1iOVSmFiYqLh3r17zaiU7du3DzQ0NHy7tLQEa40V32EdaAiIRqP7enp6vo5Go1ttNttsW1tbh1KpjITDYSRITA+NiMfj4gVN01u7urraQ6HQO9j9AwcO/OV2u/swrUAgAMgm3ldVVa2mF4vF9Ldv3/5qdHT0Q+zPhQsXQC6Xa1OplDIYDLLLy8uikCmKQlkBYbFYYHBwsMHn83nRW3t7O5w5cwa9fhKPx/cha/h4jT2sTZpKpQw+n+8oCvzcuXNw7NgxSKVSohIeP378pU6n24K0p9PpPzQaTVykvaur6wOSJFfq6uqESCQicBwnXLlyRTCbzdgU0YqKilaamppuDQwMmILBIMD169c/pyhKuHnzpoDnzp07gl6vF9RqtbBr1y7hyJEjQnFxsQhubW1tDwQClBTZsdlsgKrG0NhUrPP8+fOQz+ehuroaWlpa4NKlS3D//v2jBw8e/JnQarVhi8WSQFYymQwkk0k4fPgwNDc3i84WFxehsbERzp49i8rZEo1GNxM8z4ftdvsMFo8sIeWVlZWg1WpBpVLB5OSkqAKUW1lZGcqMJGQy2ey2bdt+Gh4e5lCDx48fB2z4/Pw8mEwmYBhGBOFvWq02YjKZZsi2traCVqudHRsbe7+oqMi+d+9ewGWDAjYYDPD69WsxQn9/PwK/q62t7SZPnjyJBSd5nv97fHy8Ri6XWyoqKgicM2zozMwMPHv2jEsmkz84nc6rDMMw0nUREsSw1Wpt8fl89TqdrtpsNn/McVzB7/cTarW61+12X0wkEikUtnRtAHE9CYLwkmGYl6WlpcULCwvfEwTB2u32cqPRuFJaWprCfYJkiZE2bdoEDodDBLvdbhz5xVgstogi1Wg0tEKh2JLJZGQURbGi9jASotELMmUwGAzhcFhhs9n+xZGRyWRJjuM4nuc1giCIq5nA8cYpRWVks9nKy5cvd3Z0dPxC0/RnDoeD0Ov1CblcnmZZVosloJF79uwBHGd08OjRo4vd3d2tCwsLjnA4bHQ6nb0URTEcx9lzuRzLsuwSRifWNkwmkyFomq4U0QQBLpeLrqqqyslkMinDMEaSJLPYYDRpSUmJSIBKpeI8Hs/vfX19jfv376fq6uoCRqOR8fv9+3ieJ3fu3Dm39gdAnjhxQiQCl4tCofjHbrdjsQlBEGYnJyetBEGodu/e/adOp0vJ5XKgKAr+A+Zwtxqkvx2BAAAAAElFTkSuQmCC"/></svg>
							</div>
						</div>
											
						<div class="cat_footer">
							<div class="cat_name">Celebrity</div>						
						</div>
					</div>
					<div class="categoryicons" style="display: none;">
						<div class="animals_main icons-category-inner animals_inner" data-catname="animals">
							<?php echo do_shortcode('[ngg src="galleries" ids="1" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="50" thumbnail_height="50" images_per_page="20" ajax_pagination="0" thumbnail_crop="0" number_of_columns="4"]');?>
						</div>
						<div class="popular_main icons-category-inner popular_inner" data-catname="popular">
							<?php echo do_shortcode('[ngg src="galleries" ids="2" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="50" thumbnail_height="50" images_per_page="20" ajax_pagination="0" thumbnail_crop="0" number_of_columns="4"]');?>
						</div>
						<div class="celebrity_main icons-category-inner celebrity_inner" data-catname="celebrity">
							<?php echo do_shortcode('[ngg src="galleries" ids="3" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="50" thumbnail_height="50" images_per_page="20" ajax_pagination="0" thumbnail_crop="0" number_of_columns="4"]');?>
						</div>
					</div>
					<div class="cat_back">Close</div>
				</div>
			   
			   
			</div>
			</div>
			</div>
	   </div>
	   <!--<div class="inside-container-bottom-close">
	      <a class="close-icon-bottom" style="padding:0p 0px 0px 0px;"><img src="https://dev.kumailenterprises.com/wp-content/uploads/2021/02/Cross.png"></a>
	   </div>-->
	</div>
		
		<!-- New Test -->
		<div class="controls" style="display: none;"> 
		  <div id="bench"></div>
		</div>
		<script>
		(function() {
		  window.addEventListener('load', function() {
		    var canvas = this.__canvas || this.canvas,
		        canvases = this.__canvases || this.canvases;

		    canvas && canvas.calcOffset && canvas.calcOffset();

		    if (canvases && canvases.length) {
		      for (var i = 0, len = canvases.length; i < len; i++) {
		        canvases[i].calcOffset();
		      }
		    }
		  });
		})();
		</script>	
		<!-- New Test -->
		
</div>
	    <!-- Add to cart and Price -->
	    <div class="section-content relative priceandcart">
			<div class="row" id="">
				<div class="col medium-7 small-7 large-7">
					<div class="col-inner">
						<div class="add-cart-btn-main">					
							<input type="submit" name="submit_prod" class="add_to_cart_btn tool-buttons-text" value="Add to Cart" />
							<span class="tool-buttons-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><metadata><x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01"><rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><rdf:description rdf:about=""></rdf:description></rdf:rdf></x:xmpmeta></metadata>
							<defs><filter id="filter" x="884" y="987" width="24" height="24" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"></feFlood><feComposite result="composite" operator="in" in2="SourceGraphic"></feComposite><feBlend result="blend" in2="SourceGraphic"></feBlend></filter></defs><path id="Forma_1_copy" data-name="Forma 1 copy" class="cls-1" d="M907.79,992.453a0.934,0.934,0,0,0-.728-0.346H890.081l-0.069-.64,0-.021A5.129,5.129,0,0,0,884.937,987a0.938,0.938,0,0,0,0,1.875,3.249,3.249,0,0,1,3.212,2.809l1.114,10.239a2.809,2.809,0,0,0-1.654,2.56v0.05a2.814,2.814,0,0,0,2.813,2.81H890.8a2.766,2.766,0,1,0,5.237,0h4.045a2.742,2.742,0,0,0-.148.89,2.766,2.766,0,1,0,2.766-2.76H890.422a0.939,0.939,0,0,1-.938-0.94v-0.05a0.939,0.939,0,0,1,.938-0.94H902.39a4.3,4.3,0,0,0,3.891-2.62,0.937,0.937,0,1,0-1.717-.75,2.446,2.446,0,0,1-2.174,1.5H891.122l-0.837-7.689h15.624l-0.459,2.2a0.939,0.939,0,0,0,.727,1.11,0.969,0.969,0,0,0,.192.019,0.938,0.938,0,0,0,.917-0.746l0.694-3.328A0.936,0.936,0,0,0,907.79,992.453ZM902.7,1007.34a0.89,0.89,0,1,1-.891.89A0.892,0.892,0,0,1,902.7,1007.34Zm-9.281,0a0.89,0.89,0,1,1-.891.89A0.891,0.891,0,0,1,893.422,1007.34Z" transform="translate(-884 -987)"></path>
							</svg></span>
						</div>		
					</div>		
				</div>
				<div id="" class="col medium-5 small-5 large-5">
					<?php
						$product_id = $post->ID;
						$product = wc_get_product( $product_id );
						$product_price_woo = $product->get_regular_price();
					?>
					<div class="col-inner">
						<div class="icon-box featured-box total_nis_btn icon-box-right text-right">
							<div class="icon-box-text last-reset">									
								<div id="text-857485485" class="text">
								<input type="hidden" name="total_price_default" id="total_price_default" value="<?php echo $product_price_woo;?>">		
								<input type="hidden" name="total_price" id="total_price" value="<?php echo $product_price_woo;?>">		
								<h3>Total - NIS <span id="total_price"><?php echo $product_price_woo;?></span></h3>		
								</div>
							</div>
						</div>
					</div>
					<img style="display: none;" id="canvastoBlob" src="">
					<input type="text" style="display: none;" id="canvas_export" value="" name="canvas_export">
				</div>
			</div>
		</div>
	</form>			
	<?php 
	return ob_get_clean();
	}

add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);

add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);
function order_meta_handler($item_id, $values, $cart_item_key) {
		// Allow plugins to add order item meta
	    ?><pre><?php print_r($cart_session); ?></pre><?php 

		$cart_session = WC()->session->get('cart');
		// if($cart_session[$cart_item_key]['rt_total_price'][0]){
		// wc_add_order_item_meta($item_id, "Total Price", $cart_session[$cart_item_key]['rt_total_price'][0]);
		// }
		if($cart_session[$cart_item_key]['selected_size'][0]){
			wc_add_order_item_meta($item_id, "Selected Size", $cart_session[$cart_item_key]['selected_size'][0]);
		}
		if($cart_session[$cart_item_key]['canvas_export'][0]){
			wc_add_order_item_meta($item_id, "Thumbnail", $cart_session[$cart_item_key]['canvas_export'][0]);
		}
	}
	function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
		global $woocommerce;
		$product_id = $post->ID;
		//$rt_total_price = get_post_meta($product_id, 'rt_total_price');
		$selected_size = get_post_meta($product_id, 'selected_size');
		$canvas_export = get_post_meta($product_id, 'canvas_export');

		return $cart_item_meta;
	}

/*
add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_single_variation_add_to_cart_button', 20 );
function woocommerce_single_variation_add_to_cart_button(){
    echo do_shortcode('[quote_generator]');
}*/